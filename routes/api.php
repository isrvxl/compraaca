<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'api\AuthController@login');
    Route::post('signup', 'api\AuthController@signup');
  
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'api\AuthController@logout');
        Route::get('user', 'api\AuthController@user');
    });
});

Route::get('/index', 'api\StoreController@listTypesStores');
Route::get('/stores/{lat}/{lng}', 'api\StoreController@listAllStores');
Route::get('/stores/type/{type}/{lat}/{lng}', 'api\StoreController@listStoresByTypes');
Route::get('/stores/category/{category}/{lat}/{lng}', 'api\StoreController@listStoresByCategory');

Route::get('/storesDetails/{id}', 'api\StoreController@detailsStore');

Route::get('/categories', 'api\CategoryController@listCategory');
Route::get('/types', 'api\CategoryController@listType');