<?php

use Illuminate\Support\Facades\Route;
use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','WebController@home')->name('home');

Auth::routes();

Route::get('/logOut', function(){
	Auth::logout();
	return redirect()->route('home');
});

Route::get('/home', 'HomeController@index');

Route::get('/dashboard', 'HomeController@index');

Route::resource('/categories', 'CategoryController');

Route::resource('/sales', 'SaleController');

Route::resource('/stores', 'StoreController');

Route::resource('/products', 'ProductController');

Route::resource('/users', 'UserController');

Route::get('clients', 'UserController@clients');

Route::get('deliverys', 'UserController@deliverys');

Route::post('/import', 'ProductController@import');

Route::get('/details/{id}', 'StoreController@details');

Route::resource('/types', 'TypeController');

/**Listar tiendas por geoposicion */
Route::post('/tiendas', 'StoreController@geoList');

Route::get('/tiendas/categoria/{id}', 'StoreController@categoryList');

Route::get('/tiendas/tipo/{id}', 'StoreController@typeList');

Route::get('/acerca_nosotros','WebController@about');

Route::get('/terminos_y_condiciones','WebController@termsConditions');

Route::post('/contactMail', 'MailController@contactMail');

Route::post('/restorantRequestMail', 'MailController@restorantRequestMail');

Route::post('/deliveryRequestMail', 'MailController@deliveryRequestMail');

Route::get('/saleTest','SaleController@saleTest');

Route::post('/flowConfirm','SaleController@flowConfirm');

Route::post('/apiFlow','SaleController@confirm');

Route::post('/flowResult','SaleController@flowResult');

Route::resource('/contact','ContactController');

Route::get('/details/{id}/cart', 'StoreController@orderCart');

Route::get('/sale/{id}/payment', 'SaleController@orderPayment')->middleware('auth');

Route::get('/sale/{id}/pay', 'SaleController@goFlow')->middleware('auth');

Route::get('/sale/{id}/confirmed', 'SaleController@orderConfirmed');

Route::get('/sale/{id}/rejected', 'SaleController@orderRejected');

Route::get('/registro_restaurante','WebController@submitRestaurant');

Route::get('/testPy','WebController@testPy');

Route::get('/registro_repartidor','WebController@submitDriver');

Route::post('/sale/details', 'SaleController@detailCart');

Route::get('/perfil','WebController@perfil')->middleware('auth');

Route::get('/getDeliveryPrice/{distance}','WebController@getDeliveryPrice');

Route::get('/sales/{id}/invoice','SaleController@invoice');

Route::get('/mySales/{id}','SaleController@invoicePerfil');
