var renderCartTable = function(cartList) {
    $('#cartTable').html("")
    var cartHtml= "";
    var total= 0;
    cartList.forEach(c => {
        cartHtml += `<tr>
            <td>
                <a href="#0" data-id="${c.id}" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>${c.qty}x</strong> ${c.name}
            </td>
            <td>
                <strong class="pull-right">$${c.totalPrice}</strong>
            </td>
        </tr>`;

        total = total + c.totalPrice;

    });


    $('#cartTable').html(cartHtml)
    $('#subTotal').text('$' + total.toString())

}

$(function() {
    
    var cartList = [];

    if( $('#storeId').val() == localStorage.getItem('storeId')){
        cartList = JSON.parse(localStorage.getItem('cartList'));
        renderCartTable(cartList);
    }

    $('body').on('click', '.goToCart', function() {
        var product = $(this).data('object');
        if(cartList.length > 0) {
            var exist =  false;
            cartList.forEach(c => {
                if(c.id === product.id) {
                    c.qty++;
                    c.totalPrice = c.totalPrice + product.price;
                    exist =  true;
                }
            });
            if(!exist) {
                product.totalPrice = product.price;
                product.qty = 1
                cartList.push(product)
            }
        }else {
            product.totalPrice = product.price;
            product.qty = 1
            cartList.push(product)
        }
        var storeId = $('#storeId').val();
        localStorage.setItem('storeId', storeId);
        localStorage.setItem('cartList', JSON.stringify(cartList));

        renderCartTable(cartList);

    })

    $('body').on('click', '.remove_item', function() {
        var productId = $(this).data('id');
        cartList = cartList.filter(c => c.id !== productId);
        localStorage.setItem('cartList', JSON.stringify(cartList));
        renderCartTable(cartList);
    })

    $('body').on('click', '.remove_list', function() {
        cartList = [];
        localStorage.clear()
        renderCartTable(cartList);
    })

    $('body').on('click', '.goToSale', function() {
        if(cartList.length > 0){

            var amount = 0;
    
            cartList.forEach(c => {
                amount = amount + c.totalPrice;
            });
    
            var data = {
                items : cartList,
                store_id : $(this).data('store'),
                amount : amount,
                _token: $("meta[name='csrf-token']").attr('content')
            }
            $.post('/sales', data, function(resp){
                location.href = '/details/'+resp.id+'/cart'
                console.log(resp);
            })
        } else {
            window.alert("Tiene que ingresar productos");
        }
    })




});