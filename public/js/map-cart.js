    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.4724727, lng: -70.9100257},
          zoom: 8
        });
       
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setComponentRestrictions(
            {'country': ['cl']});
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
        autocomplete.addListener('place_changed', function() {
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          document.getElementById('lat').value = place.geometry.location.lat();
          document.getElementById('lng').value = place.geometry.location.lng();
          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          

          var data = $('#goToPay').serializeObject();
          var origin = new google.maps.LatLng(data.storeLat, data.storeLng);
          var destination = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
           var service = new google.maps.DistanceMatrixService();
          service.getDistanceMatrix({
              origins: [origin],
              destinations: [destination],
              travelMode: 'DRIVING',
          }, function(response, status){
              if (status == 'OK') {
                  var results = response.rows[0].elements;
                  var element = results[0];
                  var distance = element.distance.value;
                  var duration = element.duration.value;
                  console.log(distance)
                  console.log(duration)
                  $.get("/getDeliveryPrice/"+ distance, {}, function(r){

                    $("#deliverPrice").text(r);
                    var price = $("#subTotal").text();
                    price = price.replace("$ ", "");
             
                    var total = parseInt(price) + parseInt(r);
                    $("#deliveryInput").val(r);
                    var price = $("#TotalPrice").text("$ "+total);
                    $( "button[type=submit]" ).prop( "disabled", false );
                    $( "button[type=submit]" ).removeClass( "disabledBtn" );
                  })
              }else{
                alert('Ha ocurrido un error intentelo mas Tarde');
              }
          });


          


          

        });



      }
    