#!/usr/bin/env python
import sys
import tarifa_delivery as td

if len(sys.argv) >= 5 and len(sys.argv)%2 == 1:
	origen = [sys.argv[1], sys.argv[2]]
	destino = [sys.argv[3], sys.argv[4]]

	pay, charge = td.delivery_fee(origen, destino)
	print([pay, charge])

else:
	print('Argumentos inválidos, deben ser mínimo 4 y una cantidad par')