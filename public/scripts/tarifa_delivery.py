# Librerias de manejo de consultas
import requests
import json

# Stack cientifico
import numpy as np
from scipy import stats

# Librerias de manejo de tiemop
from datetime import date, datetime
from time import sleep

# Manejo de funciones
from functools import singledispatch, update_wrapper

#Visualización
import matplotlib.pyplot as plt

#Libreria de consultas Google Maps
import googlemaps

class ConsultorGoogleMaps:
    
    #Atributos fijos para consultas
    __key = 'AIzaSyBUbCtO9f9KfbX0_4LDQvb-kVjkeQaNmtI'
    __client = googlemaps.Client(key = __key)
    
    def distancias(self,origenes, destinos):
        matrix = self.__client.distance_matrix(origenes, destinos, departure_time='now')
        matrix = matrix['rows']
        distances = [ [ (np.round(destination['distance']['value']/1000, decimals=3),
                         np.round(destination['duration']['value']/60, decimals=2),
                         np.round(destination['duration_in_traffic']['value']/60, decimals=2)
                         ) for destination in origin['elements'] ] for origin in matrix ]
        
        return distances
    
    def direcciones(self, origen, destino, paradas):
        paradas = ['via:'+parada for parada in paradas]
        matrix = self.__client.directions(origen, destino, waypoints=paradas,
                                          departure_time='now',
                                          region='cl',
                                          optimize_waypoints=True
                                         )
        
        legs = matrix[0]['legs']
        params = np.array([0,0,0])
        for leg in legs:
            leg_vals = np.array([ leg['distance']['value'], leg['duration']['value'], leg['duration_in_traffic']['value'] ])
            params = np.add(params,leg_vals)
        
        params = np.round(np.multiply(params, np.array([1/1000,1/60,1/60]) ), decimals=2)
        
        return params
        
def delivery_fee(origen, destino, paradas=[]):
    consultor = ConsultorGoogleMaps()
    params = consultor.direcciones(origen, destino, paradas)
    
    n_waypoints = len(paradas)
    
    km = params[0]
    km_factor = 1 + (km//5)*0.1
    
    print('Parametros: \n')
    print('Kilometros:', km, 'km')
    print('Factor de kilometros:', km_factor)
    print('Tiempo promedio:', params[1], 'mins')
    print('Tiempo en tráfico actual:', params[2], 'mins')
    print('Paradas extra:',n_waypoints, 'paradas')
    
    now = datetime.now()
    
    hour = now.hour
    hour_factor = 1.15 if hour < 8 or hour >= 21 else 1
    print('Hora:',hour,'Factor:',hour_factor)
    
    weekday = now.weekday()
    weekend_factor = 1.1 if weekday >= 4 else 1
    print('Dia de la semana:',weekday,'Factor:',weekend_factor)
    
    time_difference = params[1] - params[2]
    if np.abs(time_difference) <= 10: #Tráfico Normal
        traffic_factor = 1
        print('Trafico Normal')
    elif time_difference > 10: #Tráfico Ligero
        traffic_factor = .9
        print('Trafico Ligero')
    else: #Tráfico Denso
        print('Trafico Denso')
        traffic_factor = 1.1
    print('Factor tráfico:',traffic_factor)
    
    waypoints_fee = 450
    min_fee = 900
    base_fee = 415
    km_fee = 250*km_factor
    
    pay = (base_fee + waypoints_fee*n_waypoints + km*km_fee)*traffic_factor*weekend_factor*hour_factor
    pay = np.round(max(pay, min_fee))
    
    charge = np.round(pay*1.1)
    
    print('Se paga al rider:',pay,'\nSe cobra al cliente:',charge)
    return pay, charge
