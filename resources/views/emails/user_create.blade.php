<!DOCTYPE html>
<html lang="en">
    <body style="margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
            <tr>
                <td align="center" bgcolor="#224CAD" style="padding: 40px 0 30px 0;color:white:font-size:28px">
                   COMPRA ACA DELIVERY
                   <td style="padding: 20px 0 30px 0;">
            </tr> 
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="color: #224CAD; font-family: Arial, sans-serif; ">
                                <b>Bienvenido a Compra Aca!</b>
                                <p>
                                    Hola, {{$f_name}}: <br>
                                    ¡Te damos la bienvenida a compraaca desde ahora puedes realizar tus compras y visitar tu perfil para saber el estado de ellas.
                                </p>
                               </td>
                        </tr>
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                <div class="card-body">
                                    <h5 class="card-title">Datos de Usuario</h5>
                                        <ul class="list-group"> 
                                            <li class="list-group-item">Nombre: <strong>{{$f_name}} {{$l_name}}</strong></li> 
                                            <li class="list-group-item">Email: <strong>{{$email_order}}</strong></li> 
                                            <li class="list-group-item">Telefono: <strong>{{$tel_order}}</strong></li>  
                                            <li class="list-group-item">Rut: <strong>{{$rut}}</strong></li> 
                                            <li class="list-group-item">Contraseña: <strong>{{$password}}</strong></li> 
                                        </ul>
                                </div> 
                            </td>
                        </tr>
                    </table>  
                </td>
            </tr>
            <tr>
                <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="rigth" style="color:black; font-family: Arial, sans-serif; font-size: 14px;">
                                © CompraAca 2020
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr> 
        </table>
    </body>
</html>