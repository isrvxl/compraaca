<!DOCTYPE html>
<html lang="en">
    <body style="margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
            <tr>
                <td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0;color:white:fon">
                   COMPRA ACA DELIVERY
                </td>
            </tr> 
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                <b>Necesidad de contacto!</b>
                               </td>
                        </tr>
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                <div class="card-body">
                                    <h5 class="card-title">Datos Correo Contacto</h5>
                                        <ul class="list-group"> 
                                            <li class="list-group-item">Nombre: <strong>{{$name}}</strong></li> 
                                            <li class="list-group-item">Email: <strong>{{$email}}</strong></li> 
                                            <li class="list-group-item">Telefono: <strong>{{$subject}}</strong></li> 
                                            <li class="list-group-item">Rut: <strong>{{$content}}</strong></li> 
                                        </ul>
                                </div>  
                            </td>
                        </tr>
                    </table>  
                </td>
            </tr>
            <tr>
                <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="rigth" style="color:black; font-family: Arial, sans-serif; font-size: 14px;">
                                © CompraAca 2020
                            </td>
                        </tr>
                    </table>
                </td>
            </tr> 
        </table>
    </body>
</html>