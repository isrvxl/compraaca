<!DOCTYPE html>
<html lang="en">
    <body style="margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
            <tr>
                <td align="center" bgcolor="#224CAD" style="padding: 40px 0 30px 0;color:white:font-size:24px">
                   COMPRA ACA DELIVERY
                   <td style="padding: 20px 0 30px 0;">
            </tr> 
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; ">
                                <b>Correo de Nueva Venta!</b>
                                <td style="padding: 20px 0 30px 0;">
                        </tr>
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                <div class="card-body">
                                    <h5 class="card-title">Datos Correo Compra</h5>
                                    <ul class="list-group"> 
                                        <li class="list-group-item">Asunto: <strong>Informacion de Venta</strong></li> 
                                        <li class="list-group-item">Nombre del Usuario: <strong>{{$nameUser}}</strong></li> 
                                        <li class="list-group-item">Email del Usuario:<strong>{{$emailUser}}</strong></li> 
                                        <li class="list-group-item">Telefono del Usuario: <strong>{{$phonelUser}}</strong></li> 
                                        <li class="list-group-item">Rut del Usuario:: <strong>{{$rutUser}}</strong></li> 
                                        <li class="list-group-item">Direccion del Despacho: <strong>{{$deliveryAddress}}</strong></li> 
                                        <li class="list-group-item">Pago Delivery: <strong>{{$deliveryPrice}}</strong></li> 
                                        <li class="list-group-item">Total de la Compra: <strong>{{$totalSale}}</strong></li> 
                                        <li class="list-group-item" ><a href="http://compraaca.com/sales/{{$saleId}}/invoice"> + Info Clickee Aqui </a></li>
                                    </ul>
                                </div> 
                            </td>
                        </tr>
                    </table>  
                </td>
            </tr>
            <tr>
                <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="rigth" style="color:black; font-family: Arial, sans-serif; font-size: 14px;">
                                © CompraAca 2020
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr> 
        </table>
    </body>
</html>