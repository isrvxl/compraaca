<!DOCTYPE html>
<html lang="en">
    <body style="margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
            <tr>
                <td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0;color:white:fon">
                   COMPRA ACA DELIVERY
                </td>
            </tr> 
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                <b>Necesidad de contacto nuevo Negocio!</b>
                               </td>
                        </tr>
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                <div class="card-body">
                                    <h5 class="card-title">Datos Correo Negocio</h5>
                                        <ul class="list-group"> 
                                            <li class="list-group-item">Nombre: <strong>{{$f_name}}</strong></li> 
                                            <li class="list-group-item">Apellido: <strong>{{$l_name}}</strong></li> 
                                            <li class="list-group-item">Correo: <strong>{{$email}}</strong></li> 
                                            <li class="list-group-item">Asunto: <strong>Crear Restaurante</strong></li> 
                                            <li class="list-group-item">Numero de Telefono: <strong>{{$phone}}</strong></li> 
                                            <li class="list-group-item">Nombre del Restaurante: <strong>{{$restaurant_name}}</strong></li> 
                                            <li class="list-group-item">Rut de comercio: <strong>{{$restaurant_rut}}</strong></li> 
                                            <li class="list-group-item">Giro: <strong>{{$giro}}</strong></li> 
                                            <li class="list-group-item">Direccion: <strong>{{$address}}</strong></li> 
                                            <li class="list-group-item">Comuna: <strong>{{$comuna}}</strong></li> 
                                            <li class="list-group-item">Ciudad: <strong>{{$restaurant_city}}</strong></li> 
                                            <li class="list-group-item">Codigo Postal: <strong>{{$restaurant_postal_code}}</strong></li> 
                                        </ul>
                                </div>
                            </td>
                        </tr>
                    </table>  
                </td>
            </tr>
            <tr>
                <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="rigth" style="color:black; font-family: Arial, sans-serif; font-size: 14px;">
                                © CompraAca 2020
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr> 
        </table>
    </body>
</html>