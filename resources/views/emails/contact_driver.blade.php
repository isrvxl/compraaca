<!DOCTYPE html>
<html lang="en">
    <body style="margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
            <tr>
                <td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0;color:white:fon">
                   COMPRA ACA DELIVERY
                </td>
            </tr> 
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                <b>Necesidad de contacto Nuevo Conductor!</b>
                                <td style="padding: 20px 0 30px 0;">
                        </tr>
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                <div class="card-body">
                                    <h5 class="card-title">Datos Correo Conductor</h5>
                                        <ul class="list-group"> 
                                            <li class="list-group-item">Nombre: <strong>{{$f_name}}</strong></li> 
                                            <li class="list-group-item">Apellido: <strong>{{$l_name}}</strong></li> 
                                            <li class="list-group-item">Correo: <strong>{{$email}}</strong></li> 
                                            <li class="list-group-item">Asunto: <strong>Quiero ser repartidor</strong></li> 
                                            <li class="list-group-item">Numero de Telefono: <strong>{{$phone}}</strong></li> 
                                            <li class="list-group-item">Tiene vehiculo: <strong>{{$vehiculo}}</strong></li> 
                                            <li class="list-group-item">Tiene papeles al dia: <strong>{{$papeles}}</strong></li> 
                                            <li class="list-group-item">Tiene moto o scooter: <strong>{{$motor}}</strong></li> 
                                            <li class="list-group-item">Tiene bicicleta: <strong>{{$bicicleta}}</strong></li> 
                                            <li class="list-group-item">Tiene licencia de conducir al dia: <strong>{{$license}}</strong></li> 
                                            <li class="list-group-item">Tiene un móvil iPhone o Android: <strong>{{$mobile}}</strong></li> 
                                        </ul>
                                </div>
                            </td>
                        </tr>
                    </table>  
                </td>
            </tr>
            <tr>
                <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="rigth" style="color:black; font-family: Arial, sans-serif; font-size: 14px;">
                                            © CompraAca 2020
                                        </td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                    </table>
                </td>
            </tr> 
        </table>
    </body>
</html>