@extends('layouts.admin')

@section('css')
    <link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/lib/advanced-datatable/css/DT_bootstrap.css"/>
@endsection

@section('content')

	<h3>
		<i class="fa fa-angle-right"></i>Lista Usuarios
		<a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-default pull-right">+ Crear Usuario</a>
	</h3>

	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif

<div class="row mb" style="padding: 20px">
	
  <!-- page start-->
	<div class="content-panel" style="padding: 20px 20px 5px;">
		<br>
		<div class="adv-table">
		<table class="display table table-bordered" id="hidden-table-info">
			<thead>
			<tr>
				<th>Img</th>
				<th>Nombre</th>
				<th>Email</th>
				<th>Rut</th>
				<th>Telefono</th>
				<th>Direccion</th>
				<th>Tipo</th>
				<th style="width: 11%">Acciones</th>
				
			</tr>
			</thead>
			<tbody>
				@foreach ($users as $u)
					<tr>
						<td>  
							<img src="/images_user/{{$u->img}}" alt="" style="width: 100px;">
						</td>
						<td>{{$u->f_name}} {{$u->l_name}}</td>
						<td>{{$u->email}}</td>
						<td>{{$u->rut}}</td>
						<td>{{$u->phone}}</td>
						<td>{{$u->address}}</td>
						<td>{{$u->type_label}}</td>
						<td>
							<button class="btn btn-default edit" data-usersid="{{$u->id}}" type="submit" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fa fa-edit"></i>
							</button>
							<form action="/users/{{ $u->id }}" method="post" style="float:right">
								@csrf
								{{ method_field('delete') }}
								<button class="btn btn-default" type="submit" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Borrar">
									<i class="fa fa-trash-o"></i>
								</button>
							</form>
						</td>
					</tr>	
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
  <!-- page end-->
</div>

@include('admin.users.create')
@include('admin.users.edit')

@endsection

@section('scripts')
    
	<script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
	<!--script for this page-->
	<script>
		$(function(){
			var oTable = $('#hidden-table-info').dataTable();
			$('body').on('click','.edit',function(){
				var id = $(this).data('usersid')
				$.get('/users/'+id, function(r){
					$('#editModal form').attr('action', "/users/"+r.id)
					$('#editModal input[name=f_name]').val(r.f_name)
					$('#editModal input[name=l_name]').val(r.l_name)
					$('#editModal select[name=type]').val(r.type)
					$('#editModal input[name=email]').val(r.email)
					$('#editModal input[name=rut]').val(r.rut)
					$('#editModal input[name=phone]').val(r.phone)
					$('#editModal input[name=address]').val(r.address)
					$('#editModal input[name=locality]').val(r.locality)
					$('#editModal select[name=store_id]').val(r.store_id)
					$('#editModal').modal()
				})
			})
			$("body").on('click', '.cancelEdit', function(e){
				e.preventDefault()
				location.reload()
			})
		})
	</script>
  
@endsection