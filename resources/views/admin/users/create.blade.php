<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Crear Usuario</h4>
            </div>
            <form action="/users" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="token" value="{{csrf_token()}}">

                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Imagen</label>
                            <input type="file" name="img" required class="form-control">
                        </div>  
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Tipo</label>
                            <select name="type" class="form-control" >
                                <option value="">Seleccione</option>
                                @foreach ($types as $k=> $c)
                                    <option value="{{$k}}">{{$c}}</option>
                                @endforeach
                            </select>
                        </div>  
                    </div>
                    <div class="form-group row">
                           
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                            <input type="text" name="f_name" required class="form-control">
                        </div>  
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Apellido</label>
                            <input type="text" name="l_name" required class="form-control">
                        </div>  
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Email</label>
                            <input type="email" name="email" required class="form-control">
                        </div>   
                        <div class="col-md-6">
                            <label class=" control-label">Rut</label>
                            <input type="text" name="rut" required class="form-control">
                        </div>  
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Password</label>
                            <input type="password" name="password" required class="form-control">
                        </div>   
                        <div class="col-md-6">
                            <label class=" control-label">Confirmar Password</label>
                            <input type="password" name="dimention"  required class="form-control">
                        </div>  
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Telefono</label>
                            <input type="text" name="phone" required class="form-control">
                        </div> 
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Direccion</label>
                            <input type="text" name="address" required class="form-control">
                        </div>   
                    </div>
  
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Localidad</label>
                            <input type="text" name="locality" required class="form-control">
                        </div>  
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Tienda</label>
                            <select name="store_id" class="form-control">
                                <option value="">Seleccione</option>
                                @foreach ($stores as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>    
                <div class="modal-footer">	 
                    <div class="">
                        <input type="submit" value="Guardar" class="btn btn-primary"  style="width:200px">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>