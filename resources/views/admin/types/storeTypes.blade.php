<div class="modal fade" id="createModalT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Crear Tipo</h4>
            </div>
            <form action="/types" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="token" value="{{csrf_token()}}">
                <input type="hidden" name="type" value="1">
                <div class="modal-body" style="padding: 20px 10px">
                    <div class="form-group row">
                        <div class=" col-sm-8">
                            <input  type="text" placeholder="Nombre" name="name" required class=" form-control">
                        </div>
                        <div class="col-sm-4">
                            <input type="submit" value="Guardar" class="btn btn-primary"  >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="col-sm-2 col-sm-2 control-label">Imagen</label>
                            <input type="file" name="img" required class="form-control">
                        </div>  
                    </div>
                </div>    
                
            </form>
            <div class="modal-footer">
                <div class="adv-table">
                    <table class="display table table-bordered" id="hidden-table-info">
                        <thead>
                        <tr>
                            <th style="width:100px;">Imagen</th>
                            <th>Nombre</th>
                            <th style="width:100px;">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($types as $t)
                                <tr>
                                    <td><img src="/images_types/{{$t->img}}" style="width:100px;"></td>
                                    <td class="text-left">{{$t->name}}</td>
                                    <td>
                                        <button class="btn btn-default editT" data-id="{{$t->id}}" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Editar">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <form action="/types/{{ $t->id }}" method="post" style="float:right">
                                            @csrf
                                            {{ method_field('delete') }}
                                            <button class="btn btn-default" type="submit" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Borrar">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>	
                            @endforeach
                        </tbody>
                    </table>
                </div>   
            </div>    
        </div>
    </div>
</div>