@extends('layouts.admin')

@section('css')
    <link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/lib/advanced-datatable/css/DT_bootstrap.css"/>
@endsection

@section('content')

<h3>
	<i class="fa fa-angle-right"></i>Lista Productos
	<a href="#" data-toggle="modal" data-target="#importModal" class="btn btn-default pull-right">Importar Excel</a>
	<a href="#" data-toggle="modal" data-target="#createModalC" class="btn btn-default pull-right">Crear Categoria</a>
	<a href="#" data-toggle="modal" data-target="#createModalT" class="btn btn-default pull-right">Crear Tipo</a>
	<a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-default pull-right">Crear Producto</a>
</h3>

	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif

<div class="row mb"  style="padding: 20px">
	<div class="content-panel" style="padding: 20px 20px 5px;">
		<div class="adv-table">
		<table class="display table table-bordered" id="hidden-table-info">
			<thead>
				<tr>
					<th>Imagen</th>
					<th>Tienda</th>
					<th>Nombre</th>
					<th>Precio</th>
					<th>Descuento</th>
					<th>Peso</th>
					<th>Dimencion</th>
					<th>Alto</th>
					<th>Largo</th>
					<th>Categoria</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($products as $p)
					<tr>
						<td><img src="/images_product/{{$p->img}}" style="width: 100px;"></td>
						<td>{{$p->store->name}}</td>
						<td>{{$p->name}}</td>
						<td>$ {{number_format ( $p->price, 0, ',', '.' ) }}</td>
						<td>$ {{number_format ( $p->discount_price, 0, ',', '.' ) }}</td>
						<td>{{$p->weigth}}</td>
						<td>{{$p->dimention}}</td>
						<td>{{$p->high}}</td>
						<td>{{$p->long}}</td>
						<td>{{$p->category ? $p->category->name : ''}}</td>
						<td>
							<button class="btn btn-default edit" data-productid="{{$p->id}}" type="submit" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fa fa-edit"></i>
							</button>
							<form action="/products/{{ $p->id }}" method="post" style="float:right" data-toggle="tooltip" data-placement="top" title="Borrar"> 
								@csrf
								{{ method_field('delete') }}
								<button class="btn btn-default" type="submit" style="background:transparent;padding: 0 12px">
									<i class="fa fa-trash-o"></i>
								</button>
							</form>
						</td>
					</tr>	
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
  <!-- page end-->
</div>

@include('admin.types.productsCreate')
@include('admin.categories.productCategories')
@include('admin.categories.productCategoriesEdit')
@include('admin.types.productsEdit')
@include('admin.products.importFiles')
@include('admin.products.create')
@include('admin.products.edit')

@endsection

@section('scripts')
    

	<script type="text/javascript" language="javascript" src="/lib/advanced-datatable/js/jquery.dataTables.js"></script>
	
	<script type="text/javascript" src="/lib/advanced-datatable/js/DT_bootstrap.js"></script>
	<script>
	$(function(){
		var oTable = $('#hidden-table-info').dataTable();
		$('body').on('click','.edit',function(){
			var id = $(this).data('productid')
			$.get('/products/'+id, function(r){
				if(r.inventary == 1) {
					$('#editModal input[name=inventary]').prop('checked', true);
				}
				$('#editModal form').attr('action', "/products/"+r.id)
				$('#editModal input[name=name]').val(r.name)
				$('#editModal input[name=price]').val(r.price)
				$('#editModal input[name=discount_price]').val(r.discount_price)
				$('#editModal input[name=weigth]').val(r.weigth)
				$('#editModal input[name=stock]').val(r.stock)
				$('#editModal input[name=dimention]').val(r.dimention)
				$('#editModal input[name=high]').val(r.high)
				$('#editModal input[name=long]').val(r.long)
				$('#editModal select[name=category_id]').val(r.category_id)
				$('#editModal select[name=store_id]').val(r.store_id)
				$('#editModal select[name=type]').val(r.type)
				$('#editModal textarea[name=description]').val(r.description)
				$('#editModal').modal()
			})
		})
	  	$("body").on('click', '.cancelEdit', function(e){
			e.preventDefault()
			location.reload()
		})

		$('body').on('click','.editC',function(){
			var id2 = $(this).data('id')
			console.log(id2);
			$.get('/categories/'+id2, function(re){
				$('#editModalC form').attr('action', "/categories/"+id2)
				$('#editModalC input[name=name]').val(re.name)
				$('#editModalC').modal()
			})
		})
	  	$("body").on('click', '.cancelEditC', function(e){
			e.preventDefault()
			location.reload()
		})
		$('body').on('click','.editT',function(){
			var id3 = $(this).data('id')
			$.get('/types/'+id3, function(res){
				$('#editModalT form').attr('action', "/types/"+id3)
				$('#editModalT input[name=name]').val(res.name)
				$('#editModalT').modal()
			})
		})
	  	$("body").on('click', '.cancelEditT', function(e){
			e.preventDefault()
			location.reload()
		})
		
	})
	</script>
  
@endsection