<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Crear Producto</h4>
            </div>
            <form action="/products" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="token" value="{{csrf_token()}}">

                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Imagen</label>
                            <input type="file" name="img" required class="form-control">
                        </div>  
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label> <input type="checkbox" style="width: 20px" class="checkbox form-control" name="inventary"><span style="margin-left: 20px;line-height: 40px;">Inventariado</span>  </label> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Categoria</label>
                            <select name="category_id" class="form-control" required >
                                <option value="">Seleccione</option>
                                @foreach ($categories as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>  
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Tipo</label>
                            <select name="type" class="form-control" required >
                                <option value="">Seleccione</option>
                                @foreach ($types as $t)
                                    <option value="{{$t->id}}">{{$t->name}}</option>
                                @endforeach
                            </select>
                        </div>  
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Tienda</label>
                            <select name="store_id" class="form-control" required >
                                <option value="">Seleccione</option>
                                @foreach ($stores as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>   
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                            <input type="text" name="name" required class="form-control">
                        </div>  
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Precio</label>
                            <input type="number" name="price" required class="form-control">
                        </div>  
                        <div class="col-md-6">
                            <label class=" control-label">Precio Descuento</label>
                            <input type="number" name="discount_price" required class="form-control">
                        </div>   
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Peso</label>
                            <input type="text" name="weigth" required class="form-control">
                        </div>  
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Dimension</label>
                            <input type="text" name="dimention" required class="form-control">
                        </div>     
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Alto</label>
                            <input type="text" name="high" required class="form-control">
                        </div> 
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Largo</label>
                            <input type="text" name="long" required class="form-control">
                        </div> 
                    </div> 
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="col-sm-2 col-sm-2 control-label">Stock</label>
                            <input type="number" name="stock" required class="form-control">
                        </div> 
                    </div> 
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="col-sm-2 col-sm-2 control-label">Descripcion</label>
                            <textarea type="text" name="description" class="form-control" required></textarea>
                        </div>  
                    </div>
                </div>    
                <div class="modal-footer">	 
                    <div class="">
                        <input type="submit" value="Guardar" class="btn btn-primary"  style="width:200px">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>