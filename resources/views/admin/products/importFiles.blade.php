<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Importar Excel</h4>
            </div>
            <form action="{{ url('/import')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="col-sm-2 col-sm-2 control-label">Tienda</label>
                            <select name="store_id" class="form-control">
                                    <option value="">Seleccione</option>
                                @foreach ($stores as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>   
                        <div class="col-md-12">
                            <label class="col-sm-2 col-sm-2 control-label">Archivo</label>
                            <input type="file" name="name" required class="form-control">
                            <br>
                            <a href="/file/plantilla_producto.xls">Plantilla Excel</a>
                        </div> 
                            
                    </div>
                </div>    
                <div class="modal-footer">	 
                    <div class="">
                        <input type="submit" value="Cargar" class="btn btn-primary"  style="width:200px">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>