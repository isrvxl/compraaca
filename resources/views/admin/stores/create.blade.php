@extends('layouts.admin')

@section('css')
    <link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/trumbowyg/dist/ui/trumbowyg.min.css">
    <link rel="stylesheet" href="/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.min.css">
@endsection

@section('content')

	<h3>
        <i class="fa fa-angle-right"></i>Crear Tienda
        <a href="#" data-toggle="modal" data-target="#createModalT" class="btn btn-default pull-right">Crear Tipo</a>
        <a href="#" data-toggle="modal" data-target="#createModalC" class="btn btn-default pull-right">Crear Categoria</a>
    </h3>
	

<div class="row mb" style="padding: 20px">
	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif
  <!-- page start-->
	<div class="content-panel" style="padding: 20px;">
		<form action="/stores" method="POST" enctype="multipart/form-data">
            @csrf
                <input type="hidden" id="lat" name="lat">
                <input type="hidden" id="lng" name="lng">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Banner</label>
                        <input type="file" name="img_backgroud"  required class="form-control">
                    </div> 
                    <div class="col-md-6">
                        <div class="checkbox">
                             <label> <input type="checkbox" style="width: 20px" class="checkbox form-control" name="is_salient"><span style="margin-left: 20px;line-height: 40px;">Destacado</span>  </label> 
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Logo</label>
                        <input type="file" name="img"  required class="form-control">
                    </div>  
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Tipo</label>
                        <select name="type" class="form-control" required id="">
                            <option value="">Seleccione</option>
                            @foreach ($types as $t)
                                <option value="{{$t->id}}">{{$t->name}}</option>
                            @endforeach
                        </select>
                    </div>  
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Categoria</label>
                        <select name="category_id" class="form-control" required id="">
                            <option value="">Seleccione</option>
                            @foreach ($categories as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                            @endforeach
                        </select>
                    </div>  
             
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Estado</label>
                        <select name="status" class="form-control" required id="">
                            <option value="">Seleccione</option>
                            <option value="1">Habilitado</option>
                            <option value="2">Desahabilitado</option>
                        </select>
                    </div>  
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                        <input type="text" name="name"required class="form-control">
                    </div>   
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Email</label>
                        <input type="email" name="email" required class="form-control">
                    </div>  
                </div>
              
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="col-sm-2 col-sm-2 control-label">Direccion</label>
                        <input type="text" id="pac-input"  name="address"  placeholder="Ingrese direccion"  required class="form-control">
                        <br>
                        <label class="col-sm-2 col-sm-2 control-label">Localidad</label>
                        <input type="text" name="locality"  required class="form-control">
                        <br>
                        <label class="col-sm-2 col-sm-2 control-label">Rut</label>
                        <input type="text" name="rut"  required class="form-control">
                        <br>
                        <label class="col-sm-2 col-sm-2 control-label">Telefono</label>
                        <input type="text" name="phone"  required class="form-control">
                        <br>
                        <label class="col-sm-2 col-sm-2 control-label">Horario</label>
                        <input type="text" name="hours"  required class="form-control">
                        <br>
                        <label class="col-sm-2 col-sm-2 control-label">Tiempo de Entrega (minutos)</label>
                        <input type="number" name="estimate_time"  required class="form-control">
                    </div>   
                    <div class="col-md-6">
                        <div id="map"></div>
                    </div>  
                </div>
                <div class="form-group row" style="padding: 0 15px">
                    <br>
                    <label class="col-sm-2 col-sm-2 control-label">Descripcion</label>
                    <br>
                    <textarea name="description" id="trumbowyg-demo" ></textarea>
                </div>

               
            
                <div class="text-right">
                    <input type="submit" value="Guardar" class="btn btn-primary"  style="width:200px">
                </div>
            
        </form>
	</div>
  <!-- page end-->
</div>
@include('admin.types.storeTypes')
@include('admin.types.storeTypesEdit')
@include('admin.categories.storeCategories')
@include('admin.categories.storeCategoriesEdit')

@endsection

@section('scripts')
    
    <script src="/js/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSY5xPLXVGwJ96DaIOn1CMqgJhMIqH89Q&libraries=places&callback=initMap" async defer></script>
    <script src="/trumbowyg/dist/trumbowyg.min.js"></script>
    <script src="/trumbowyg/dist/plugins/lineheight/trumbowyg.lineheight.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
    <script src="/trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js"></script>
    <script src="/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js"></script>
    <script type="/text/javascript" src="/trumbowyg/dist/langs/es.min.js"></script>
    

	<script>
	$(function(){
        $('#trumbowyg-demo').trumbowyg({
            lang: 'es',
            btns: [
                
                ['fontfamily','fontsize','strong', 'em', 'del'],
                ['foreColor', 'backColor'],
                ['link'],
                ['insertImage','upload'],
                ['unorderedList', 'orderedList'],
                ['viewHTML'],
                ['undo', 'redo'], // Only supported in Blink browsers
                ['formatting'],
                ['horizontalRule'],
                ['lineheight','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['fullscreen'],
            ],
            plugins: {
                upload: {
                    serverPath:"http://167.99.118.90/contentImage"
                }
            }
        });

        $('body').on('click','.editC',function(){
			var id2 = $(this).data('id')
			console.log(id2);
			$.get('/categories/'+id2, function(re){
				$('#editModalC form').attr('action', "/categories/"+id2)
				$('#editModalC input[name=name]').val(re.name)
				$('#editModalC').modal()
			})
		})
	  	$("body").on('click', '.cancelEditC', function(e){
			e.preventDefault()
			location.reload()
		})
		$('body').on('click','.editT',function(){
			var id3 = $(this).data('id')
			$.get('/types/'+id3, function(res){
				$('#editModalT form').attr('action', "/types/"+id3)
				$('#editModalT input[name=name]').val(res.name)
				$('#editModalT').modal()
			})
		})
	  	$("body").on('click', '.cancelEditT', function(e){
			e.preventDefault()
			location.reload()
		})

	})
	</script>
  
@endsection