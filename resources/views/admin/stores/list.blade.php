@extends('layouts.admin')

@section('css')
    <link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/lib/advanced-datatable/css/DT_bootstrap.css"/>
	<style>
		tr.odd{
			background-color: white
		}
	</style>
@endsection

@section('content')

	<h3>
	
		<i class="fa fa-angle-right"></i>Lista Tiendas
		<a href="/stores/create"  class="btn btn-default pull-right">
			+ Crear Tienda
		</a>
	</h3>

	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif

<div class="row mb" style="padding: 20px">
	
  <!-- page start-->
	<div class="content-panel" style="padding: 20px 20px 5px;">
		<div class="adv-table">
		<table class="display table table-bordered" id="hidden-table-info">
			<thead>
			<tr>
				<th>Img</th>
				<th>Nombre</th>
				<th>Email</th>
				<th>Telefono</th>
				<th>Direccion</th>
				<th>Localidad</th>
				<th>Tipo</th>
				<th>Categoria</th>
				<th>Estado</th>
				<th>Acciones</th>
			</tr>
			</thead>
			<tbody>
				@foreach ($stores as $s)
					<tr>
						<td><img src="/images_store/{{$s->img}}" style="width: 100px;"></td>
						<td>{{$s->name}}</td>
						<td>{{$s->email}}</td>
						<td>{{$s->phone}}</td>
						<td>{{$s->address}}</td>
						<td>{{$s->locality}}</td>
						<td>{{$s->tipo->name}}</td>
						<td>{{$s->category->name}}</td>
						<td>{{$s->status_label}}</td>
						<td>
							<a class="btn btn-default" href="{{ route('stores.edit', $s->id) }}" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fa fa-edit"></i>
							</a>
							<form action="/stores/{{ $s->id }}" method="post" style="float:right">
								@csrf
								{{ method_field('delete') }}
								<button class="btn btn-default" type="submit" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Borrar">
									<i class="fa fa-trash-o"></i>
								</button>
							</form>
						</td>
					</tr>	
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
  <!-- page end-->
</div>



@endsection

@section('scripts')
    

	<script type="text/javascript" language="javascript" src="/lib/advanced-datatable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/lib/advanced-datatable/js/DT_bootstrap.js"></script>
	<script>
	$(function(){
		var oTable = $('#hidden-table-info').dataTable();
	})
	</script>
  
@endsection