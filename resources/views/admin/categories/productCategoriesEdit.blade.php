<div class="modal fade" id="editModalC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Editar Categoria</h4>
            </div>
            <form action="/categories/$id" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <input type="hidden" id="token" value="{{csrf_token()}}">
                <input type="hidden" name="type" value="2">

                <div class="modal-body" style="padding: 20px 30px">
                    <div class="form-group row">
                            <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                            <input type="text" name="name" required class="form-control">
                            <br>    
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="col-sm-2 col-sm-2 control-label">Imagen</label>
                            <input type="file" name="img" required class="form-control">
                        </div>  
                    </div>
                </div>    
                <div class="modal-footer">	 
                    <div class="">
                        <input type="submit" value="Actualizar" class="btn btn-primary"  style="width:200px">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>