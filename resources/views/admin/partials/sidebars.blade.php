<aside>
    <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          	<p class="centered"><a href="/"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          	<h5 class="centered">Admin</h5>
          	<li class="mt">
            	<a class="{{  $menu === 1 ? 'active': '' }}" href="/dashboard">
              		<i class="fa fa-dashboard"></i>
              		<span>Dashboard</span>
              	</a>
          	</li>
          	<li class="sub-menu">
            	<a class="{{  $menu === 2 ? 'active': '' }}">
              		<i class="fa fa-sitemap"></i>
              		<span>Tiendas</span>
              	</a>
            	<ul class="sub">
					  <li><a href="/stores">Ver Tiendas</a></li>
					  <li><a href="/stores/create">Crear Tienda</a></li>
            	</ul>
          	</li>
          	<li class="sub-menu">
            	<a class="{{  $menu === 3 ? 'active': '' }}">
              		<i class="fa fa-shopping-cart"></i>
              		<span>Productos</span>
              	</a>
            	<ul class="sub">
              		<li><a href="/products">Ver Productos</a></li>
            	</ul>
			</li>
			<li class="sub-menu">
            	<a class="{{  $menu === 4 ? 'active': '' }}" href="/sales">
              		<i class="fa fa-dollar"></i>
              		<span>Ventas</span>
              	</a>

          	</li>
			<li class="sub-menu">
            	<a class="{{  $menu === 5 ? 'active': '' }}" href="/users">
              		<i class="fa fa-users"></i>
              		<span>Usuarios</span>
              	</a>
            	<!--ul class="sub">
					<li><a href="/clients">Ver Clientes</a></li>
					<li><a href="/users">Ver Usuarios</a></li>
					<li><a href="/delivery">Ver Repartidores</a></li>
            	</ul-->
          	</li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
