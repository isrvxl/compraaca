<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Info Venta</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4" >
                        <label class="control-label">Detalles del Usuario</label>
                        <ul class="list-group"> 
                            <li class="list-group-item">Nombre:  <strong id="nameUser"></strong></li> 
                            <li class="list-group-item">Email:  <strong id="emailUser"></strong></li> 
                            <li class="list-group-item">Telefono:  <strong id="phoneUser"></strong></li> 
                            <li class="list-group-item">Rut  <strong id="rutUser"></strong></li> 
                            <li class="list-group-item">Direccion:  <strong id="addressUser"></strong></li> 
                        </ul>
                    </div>  
                    <div class="col-md-4">
                        <label class="control-label">Detalles de la Tienda</label>
                        <ul class="list-group"> 
                            <li class="list-group-item">Nombre:  <strong id="nameStore"></strong></li> 
                            <li class="list-group-item">Email:  <strong id="emailStore"></strong></li> 
                            <li class="list-group-item">Direccion:  <strong id="addressStore"></strong></li> 
                            <li class="list-group-item">Telefono  <strong id="phoneStore"></strong></li> 
                            <li class="list-group-item">Horario:  <strong id="hoursStore"></strong></li> 
                        </ul>
                    </div>  
                    <div class="col-md-4">
                        <label class="control-label">Detalles de la Compra</label>
                        <ul class="list-group"> 
                            <li class="list-group-item">Direccion de la Entrega:  <strong id="deliveryAddress"></strong></li> 
                            <li class="list-group-item">Nombre de Contacto:  <strong id="contactName"></strong></li> 
                            <li class="list-group-item">Telefono de Contacto:  <strong id="contactPhone"></strong></li> 
                            <li class="list-group-item">Total de la Compra:  <strong id="totalSale"></strong></li> 
                        </ul>
                    </div>  
                </div>  
            </div>    
        </div>
    </div>
</div>