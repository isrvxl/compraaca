<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Editar Estado</h4>
            </div>
            <form action="/sales/$id" method="POST">
                @method('PUT')
                @csrf
                <input type="hidden" id="token" value="{{csrf_token()}}">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="col-sm-2 col-sm-2 control-label">Estado</label>
                            <select name="status" class="form-control">
                                <option value="">Seleccione</option>
                                @foreach ($status as $k=> $c)
                                    <option value="{{$k}}">{{$c}}</option>
                                @endforeach
                            </select>
                        </div>   
                    </div>  
                </div>    
                <div class="modal-footer">	 
                    <div class="">
                        <input type="submit" value="Actualizar" class="btn btn-primary"  style="width:200px">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>