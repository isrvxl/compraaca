@extends('layouts.admin')

@section('css')
    <link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
    <link href="/lib/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/lib/advanced-datatable/css/DT_bootstrap.css"/>
	
	<style>
	.list-group-item {
		border: none;
	}
	</style>
@endsection

@section('content')

<h3>
	<i class="fa fa-angle-right"></i>Lista Ventas
</h3>

	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif

<div class="row mb"  style="padding: 20px">
	<div class="content-panel" style="padding: 20px 20px 5px;">
		<div class="adv-table">
		<table class="display table table-bordered" id="hidden-table-info">
			<thead>
				<tr>
					<th>Usuario</th>
					<th>Tienda</th>
					<th>Total</th>
					<th>Estado</th>
					<th>Fecha</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($sale as $s)
					<tr>
						<td>{{$s->user->f_name ?? ''}} {{$s->user->l_name ?? ''}}</td>
						<td>{{$s->store->name}}</td>
						<td>$ {{number_format ( $s->amount, 0, ',', '.' ) }}</td>
						<td>{{$s->statuslabel}}</td>
						<td>{{$s->created_at->toDateString()}}</td>
						<td data-id="{{$s->id}}">
							<button class="btn btn-default edit" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fa fa-edit"></i>
							</button>
							<a class="btn btn-default" href="/sales/{{$s->id}}/invoice" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Mas info">
								<i class="fa fa-eye"></i>
							</a>
						</td>
					</tr>	
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
  <!-- page end-->
</div>

@include('admin.sales.edit')
@include('admin.sales.info')


@endsection

@section('scripts')
    
<script type="text/javascript" language="javascript" src="/lib/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/lib/advanced-datatable/js/DT_bootstrap.js"></script>
<script>
$(function(){
	var oTable = $('#hidden-table-info').dataTable();

	$('body').on('click','.edit',function(){
	var id = $(this).parent().data('id')
		$.get('/sales/'+id, function(r){
			$('#editModal form').attr('action', "/sales/"+r.id)
			$('#editModal select[name=status]').val(r.status)
			$('#editModal').modal()
		})
	})
		$("body").on('click', '.cancelEdit', function(e){
			e.preventDefault()
			location.reload()
	})

	$('body').on('click','.info',function(){
	var id = $(this).parent().data('id')
		$.get('/sales/'+id, function(r){
			$('#infoModal form').attr('action', "/sales/"+r.id)
			$('#infoModal #nameUser').text(r.user.f_name + r.user.l_name)
			$('#infoModal #emailUser').text(r.user.email)
			$('#infoModal #phoneUser').text(r.user.phone)
			$('#infoModal #rutUser').text(r.user.rut)
			$('#infoModal #addressUser').text(r.user.address)
			$('#infoModal #nameStore').text(r.store.name)
			$('#infoModal #emailStore').text(r.store.email)
			$('#infoModal #addressStore').text(r.store.address)
			$('#infoModal #phoneStore').text(r.store.phone)
			$('#infoModal #hoursStore').text(r.store.hours)
			$('#infoModal #deliveryAddress').text(r.details.delivery_address)
			$('#infoModal #contactName').text(r.details.contact_name)
			$('#infoModal #contactPhone').text(r.details.contact_phone)
			$('#infoModal #totalSale').text(r.amount)
			$('#infoModal').modal()
		})
	})
		$("body").on('click', '.cancelEdit', function(e){
			e.preventDefault()
			location.reload()
	})

})
</script>
  
@endsection