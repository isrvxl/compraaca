<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
                            <div class="invoice-body">
                                <div class="pull-left">
                                    <h1>Estado Compra</h1>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-9">
                                        <address>
                                        <strong>Nombre Tienda</strong><br>
                                        <abbr title="Phone">P:</abbr>Tienda Numero
                                        </address>
                                    </div>
                                    <!-- /col-md-9 -->
                                    <div class="col-md-3">
                                        <br>
                                        <div>
                                            <!-- /col-md-3 -->
                                            <div class="pull-left"> Fecha Compra: </div>
                                            <div class="pull-right"> 12/08/2020 </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- /row -->
                                        <br>
                                    </div>
                                    <!-- /invoice-body -->
                                </div>
                            <!-- /col-lg-10 -->
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="text-left">Producto</th>
                                        <th class="text-left">Direccion Entrega</th>
                                        <th class="text-left">Nombre</th>
                                        <th class="text-right">Descripcion</th>
                                        <th style="width:90px" class="text-right">TOTAL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($salesD as $s)
                                        <tr>
                                            <td>Nombre</td>
                                            <td class="text-center">{{$s->delivery_address}}</td>
                                            <td>{{$s->contact_name}}</td>
                                            <td class="text-right">$429.00</td>
                                            <td class="text-right">{{$s->amount}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="4">
                                            <h4>Terminos y Condiciones</h4>
                                            <td></td>
                                            <td class="text-right"><strong>Subtotal</strong></td>
                                            <td class="text-right">$1029.00</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="text-right no-border">
                                            <div class="well well-small green"><strong>Total</strong></div>
                                            </td>
                                            <td class="text-right"><strong>$1029.00</strong></td>
                                        </tr>   
                                        @endforeach
                                    </tbody>
                                </table>
                                <br>
                                <br>
                            </div>
      </div>
    </div>
  </div>