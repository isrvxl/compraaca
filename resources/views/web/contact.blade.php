<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title" id="myModalLabel">Contacto</h2>
            </div>
            <form action="/contactMail" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" name="name" required class="form-control form-white" placeholder="Nombre">
                        </div>  
                        <div class="col-md-6">
                            <input type="email" name="email" required class="form-control form-white" placeholder="Email">
                        </div>   
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="text" name="subject" required class="form-control form-white" placeholder="Asunto">
                        </div>    
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <textarea type="text" name="content" class="form-control form-white" required placeholder="Mensaje"></textarea>
                        </div>  
                    </div>
                </div>    
                <div class="modal-footer">	 
                    <div class="">
                        <button type="submit" class="btn btn-submit">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- End modal -->   