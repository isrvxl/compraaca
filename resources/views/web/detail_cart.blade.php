@extends('layouts.web')

@section('content')
<style>
	.disabledBtn {
		opacity: 0.3;
	}
</style>

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Haga su Pedido</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step active">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong>Sus Datos</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#0" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step disabled">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong>Pago</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_2.html" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step disabled">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong>Terminado!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_3.html" class="bs-wizard-dot"></a>
                </div>  
		</div><!-- End bs-wizard --> 
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->


<!-- Content ================================================== -->
<div class="container margin_60_35">
		<div class="row">
			<form  id="goToPay" action="/sale/details" method="POST">
			<div class="col-md-3">
            
				<div class="box_style_2 hidden-xs info ">
					<h4 class="nomargin_top">Plazo de Entrega<i class="icon_clock_alt pull-right"></i></h4>
					<p>
						Tienda maneja un tiempo estimado de {{$sale->store->estimate_time ?? 30 }} min. Para entregar su pedido
					</p>
					<hr>
					
				</div><!-- End box_style_1 -->
				<div class="box_style_2 hidden-xs info ">
					<h4 class="nomargin_top">Login<i class=" pull-right"></i></h4>
					<p>
						Ya tienes una cuentra creada?
					</p>
					<p>
						Haz click aqui <a href="#0" data-toggle="modal" data-target="#login_2">Login</a>
					</p>
					<hr>
					
				</div><!-- End box_style_1 -->
                
				<div class="box_style_2 hidden-xs" id="help">
					<i class="icon_lifesaver"></i>
					<h4>Necesitas <span>Ayuda?</span></h4>
					<a href="tel://56988985377" class="phone">+569-8898-5377</a>
					<h5>Horario</h5>
					<small> 10:00 - 22:00</small>
				</div>
                
			</div><!-- End col-md-3 -->
				<div class="col-md-6">
					
						@csrf
						<input type="hidden" name="storeLat" value="{{$sale->store->lat}}">
						<input type="hidden" name="storeLng" value="{{$sale->store->lng}}">
						<input type="hidden" name="lat" id="lat">
						<input type="hidden" name="lng" id="lng">
						<div class="box_style_2" id="order_process">
							<h2 class="inner">Detalles de Usuario</h2>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Nombre</label>
									<input type="text" required class="form-control" name="f_name" {{ $user ? "readonly value=".$user->f_name." " : ''}}>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Apellido </label>
										<input type="text" required class="form-control" name="l_name" {{ $user ? "readonly value=".$user->l_name." " : ''}}>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Telefono/Movil</label>
										<input type="text" required name="tel_order" class="form-control"  {{ $user ? "readonly value=".$user->phone." " : ''}}>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" required name="email_order" class="form-control"  {{ $user ? "readonly value=".$user->email." " : ''}}>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Rut</label>
										<input type="text" required name="rut" oninput="checkRut(this)" class="form-control"  {{ $user ? "readonly value=".$user->rut." " : ''}}>
									</div>
								</div>
								@if (!$user)
								<div class="col-md-6">
									<div class="form-group">
										<label>Password</label>
										<input type="password" required name="password" class="form-control" >
									</div>
								</div>
								@endif
								
							</div>
							<hr>
							<br>
							
						</div><!-- End box_style_1 -->
						<div class="box_style_2" id="order_process">
							<h2 class="inner">Detalles de su Pedido</h2>
							<div class="row">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Direccion</label>
											<input type="text" id="pac-input" name="address" required class="form-control">
										</div>
									</div>
									<div class="col-md-12">
										<div id="map"></div>
									</div>							
								</div>	
								<hr>
								<div class="row">
									<div class="col-md-12">
											<label>Notas para el restaurante</label>
											<textarea class="form-control" style="height:150px" name="notes" id="notes"></textarea>
									</div>
								</div>
							</div>
						</div><!-- End box_style_1 -->

				</div><!-- End col-md-6 -->
			<div class="col-md-3" id="sidebar">
            	<div class="theiaStickySidebar">
				<div id="cart_box">
					<h3>Tu orden<i class="icon_cart_alt pull-right"></i></h3>
					<table class="table table_summary">
					<tbody id="cartTable">
						@foreach ($sale->saleitems as $item)
							<tr>
								<td>
								 <strong>{{$item->qty}}</strong> {{$item->product->name}}
								</td>
								<td>
									<strong class="pull-right">$ {{ $item->amount }}</strong>
								</td>
							</tr>
						@endforeach
					</tbody>
					</table>
					<hr>
					<div method="POST" >
					<table class="table table_summary">
					<tbody>
					<tr>
						<td>
							Subtotal <span id="subTotal" class="pull-right">$ {{ $sale->amount }}</span>
					   </td>
					</tr>
					<tr>
						<td>
							Valor Delivery<span id="deliverPrice" class="pull-right"></span>
						</td>
					</tr>
					<tr>
						<td class="total">
							 TOTAL <span class="pull-right" id="TotalPrice">$ {{ $sale->amount  }}</span>
						</td>
					</tr>
					</tbody>
					</table>
					<hr>

						<input type="hidden" name="store_id" value="{{$sale->store_id}}">
						<input type="hidden" name="sale_id" value="{{$sale->id}}">
						<input id="deliveryInput" type="hidden" name="delivery_price" value="">
				</div>
					<button type="submit" class="btn_full disabledBtn" disabled >Ir a pagar</button>
					
					<a class="btn_full_outline" href="/details/{{$sale->store_id}}"><i class="icon-right"></i>Añadir otros items</a>
				</div><!-- End cart_box -->
				</div><!-- End theiaStickySidebar -->
			</form>
			</div><!-- End col-md-3 -->
            
		</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->

@endsection

@section('scripts')
    
<!-- SPECIFIC SCRIPTS -->
<script src="/web/js/ResizeSensor.min.js"></script>
<script src="/web/js/theia-sticky-sidebar.min.js"></script>
<script>
    jQuery('#sidebar').theiaStickySidebar({
      additionalMarginTop: 80
    });
</script>
<script src="/js/map-cart.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSY5xPLXVGwJ96DaIOn1CMqgJhMIqH89Q&libraries=places&callback=initMap" async defer></script>
<script src="/js/sales.js"></script>
<script src="/js/validarRUT.js"></script>
@endsection