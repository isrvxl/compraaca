@extends('layouts.web')

@section('css')
	<link href="/web/css/skins/square/grey.css" rel="stylesheet">
@endsection

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
        <h1>Trabaja con nosotros</h1>
         <p>Comienza a incrementar tus ingresos</p>
         <p></p>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
    <div class="main_title margin_mobile">
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
			
            <h2 class="nomargin_top">Trabajo flexible y excelentes tarifas</h2>
            <p>
                ¿Quieres ser tu propio jefe e incrementar tus ganancias?
            </p>
        </div>
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
			<div class="feature_2">
				<i class="icon_currency"></i>
				<h3><span>Buenas</span> tarifas</h3>
				<p>
					Tarifas acordes a tus funciones y repartos, por sobre la media de la competencia y con pagos diarios y semanales según modalidad.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.2s">
			<div class="feature_2">
				<i class="icon_easel"></i>
				<h3><span>Aumenta</span> tus ingresos</h3>
				<p>
					Al ser un trabajo de tiempo flexible, puedes complementar tus rentas con estos ingresos extras, suamando mas dinero a tus finanzas personales
				</p>
			</div>
		</div>
	</div><!-- End row -->
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.3s">
			<div class="feature_2">
				<i class="icon_mobile"></i>
				<h3><span>Administra</span> tus repartos a través de la App</h3>
				<p>
					Una app de fácil acceso y uso, te brindará la posibilidad de administrar tus repartos según la cercanía de tu hogar, tu organizas tus tiempos de conexión.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.4s">
			<div class="feature_2">
				<i class="icon_map_alt"></i>
				<h3><span>Trabaja</span> en un área pequeña</h3>
				<p>
					Si deseas te mueves dentro de tu barrio, lugar conocido y seguro para ti y tu decides el área y rango de distancia donde trabajar, según tu comodidad y seguridad..
				</p>
			</div>
		</div>
	</div><!-- End row -->
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.5s">
			<div class="feature_2">
				<i class="icon_clock_alt"></i>
				<h3><span>Horas</span> flexible</h3>
				<p>
					Trabaja todas las mañanas, sólo las tardes, todo el día o solo los fines de semana. Tu decides y organizas tu jornada, las ganancias las generas como tu decidas.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.6s">
			<div class="feature_2">
				<i class="icon_calendar"></i>
				<h3><span>Días</span> flexibles</h3>
				<p>
					Al igual que las jornadas, tu organizas los días que quieres o puedes trabajar, son tu decisión acordé a tu disponibilidad. Las horarios nocturnos son de mejor paga.
				</p>
			</div>
		</div>
	</div><!-- End row -->
</div><!-- End container -->

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 nopadding features-intro-img">
			<div class="features-bg img_2">
				<div class="features-img">
				</div>
			</div>
		</div>
		<div class="col-md-6 nopadding">
			<div class="features-content">
				<h3>"Lo que necesitarás"</h3>
				<ul class="list_ok">
                	<li>Movilización propia, ya sea bicicleta o Motocicleta o Automóvil con documentación al día.</li>
                    <li>Documentación personal al dia.</li>
					<li>Permiso para trabajar en Chile.</li>
					<li>Contar con un teléfono Android o iPhone 5.0 o mas.</li>
                </ul>
			</div>
		</div>
	</div>
</div><!-- End container-fluid  -->

<div class="container margin_60">
	 <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Por favor llene el formulario a continuación</h2>
            <p>
                Comienza a ganar dinero extra diariamente
            </p>
        </div>
	<div class="row">
    	<div class="col-md-8 col-md-offset-2">
            <form action="/deliveryRequestMail" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="f_name">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" class="form-control" name="l_name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label>Email:</label>
                            <input type="email" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label>Numero de Telefono:</label>
                            <input type="text" name="phone" class="form-control">
                        </div>
                    </div>
                </div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
                                	<h5>¿Tienes vehiculo?</h5>
									<label><input name="vehiculo" type="radio" value="Si" class="icheck" checked>Si</label>
                                    <label class="margin_left"><input name="vehiculo" type="radio" value="No" class="icheck">No</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                	<h5>¿Tienes papeles al dia?</h5>
									<label><input name="papeles" type="radio" value="Si" class="icheck" checked>Si</label>
                                    <label class="margin_left"><input name="papeles" type="radio" value="No" class="icheck">No</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                	<h5>¿Tienes moto o scooter?</h5>
									<label><input name="motor" type="radio" value="Si" class="icheck" checked>Si</label>
                                    <label class="margin_left"><input name="motor" type="radio" value="No" class="icheck">No</label>
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group">
                                	<h5>¿Tienes bicicleta?</h5>
									<label><input name="bicicleta" type="radio" value="Si" class="icheck" checked>Si</label>
                                    <label class="margin_left"><input name="bicicleta" type="radio" value="No" class="icheck">No</label>
								</div>
							</div>
						</div><!-- End row  -->
                        <div class="row">
							<div class="col-md-6">
								<div class="form-group">
                                	<h5>¿Tienes licencia de conducir al dia?</h5>
									<label><input name="license" type="radio" value="Si" class="icheck" checked>Si</label>
                                    <label class="margin_left"><input name="license" type="radio" value="No" class="icheck">No</label>
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group">
                                	<h5>¿Tienes un móvil iPhone o Android?</h5>
									<label><input name="mobile" type="radio" value="Si" class="icheck" checked>Si</label>
                                    <label class="margin_left"><input name="mobile" type="radio" value="No" class="icheck">No</label>
								</div>
							</div>
						</div><!-- End row  -->
                        <hr style="border-color:#ddd;">
                        <div class="text-center"><button class="btn_full_outline">Enviar</button></div>
					</form>
        </div><!-- End col  -->
    </div><!-- End row  -->
</div><!-- End container  -->
<!-- End Content =============================================== -->

@endsection