@extends('layouts.web')

@section('content')


<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
	<div id="sub_content">
    	<h1>{{count($stores)}} resultados cercanos</h1>
        <div><i class="icon_pin"></i>{{$address ?? ''}}</div>
    </div><!-- End sub_content -->
</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

    <div class="collapse" id="collapseMap">
		<div id="map" class="map"></div>
	</div><!-- End Map -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
    
		<div class="col-md-3">
			<p>
				<a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" >Ver en Mapa</a>
			</p>
			<div id="filters_col">
				<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filtros <i class="icon-plus-1 pull-right"></i></a>
				<div class="collapse" id="collapseFilters">
					<div class="filter_type">
                    	
						<h6>Tipos</h6>
						<ul>
							<li><label><input type="checkbox" checked class="icheck">Todos <small>({{count($stores)}})</small></label></li>
							@foreach ($types as $t)
								<li><label><a href="/tiendas/tipo/{{$t->id}}"><input type="checkbox" data-id="{{$t->id}}" class="icheck">{{$t->name}}</a></label><i class="color_1"></i></li>
							@endforeach
						</ul>
					</div>
					<div class="filter_type">
						<h6>Sector</h6>
						<ul class="nomargin">
							@foreach ($categories as $c)
								<li><label><a href="/tiendas/categoria/{{$c->id}}"><input type="checkbox" class="icheck">{{$c->name}}</a></label></li>
							@endforeach
						</ul>
					</div>
				</div><!--End collapse -->
			</div><!--End filters col-->
		</div><!--End col-md -->
        
		<div class="col-md-9">
        
        <div id="tools">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="styled-select">
							<select name="sort_rating" id="sort_rating">
								<option value="" selected>Ordenar por ranking</option>
								<option value="lower">Mayor ranking</option>
								<option value="higher">Menor ranking</option>
							</select>
						</div>
					</div>
				</div>
			</div><!--End tools -->
			@foreach ($stores as $s)
                <div class="strip_list wow fadeIn" data-wow-delay="0.1s">
					@if ($s['is_salient'] == 1)
						<div class="ribbon_1">
							Destacado
						</div>	
					@endif
                    
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <div class="desc">
                                <div class="thumb_strip">
									<a href="/details/{{$s['id']}}">
										<img src="/images_store/{{$s['img']}}" alt="">
									</a>
                                </div>
                                <div class="rating">
									<i class="icon_star voted"></i>
									<i class="icon_star voted"></i>
									<i class="icon_star voted"></i>
									<i class="icon_star voted"></i>
									<i class="icon_star"></i> 
                                </div>
                                <h3>{{$s['name']}}</h3>
                                <div class="type">
                                    {{ is_array($s) ? $s['type_label'] : $s->tipo->name  }}
                                </div>
                                <div class="location">
									{{$s['address']}} <br>
									<span class=""><strong>Horario: {{ $s['hours'] }}.</strong></span>
									<span class=""><strong>Distancia: {{ number_format($s['distance'], 1) }}km.</strong></span>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="go_to">
                                <div>
                                    <a href="/details/{{$s['id']}}" class="btn_1">Ver Tienda</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- End row-->
                </div><!-- End strip_list-->
            @endforeach

            <a href="#0" class="load_more_bt wow fadeIn" data-wow-delay="0.2s">Load more...</a>  
		</div><!-- End col-md-9-->
        
	</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->


@endsection

@section('scripts')

<!-- SPECIFIC SCRIPTS -->
<script  src="/web/js/cat_nav_mobile.js"></script>
<script>$('#cat_nav').mobileMenu();</script>
<script>
	
	
	window.markersData = JSON.parse(`<?php echo json_encode($mapStore) ?>`);
	window.lat = `<?php echo $lat ?>`;
	window.lng = `<?php echo $lng ?>`;
	
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSY5xPLXVGwJ96DaIOn1CMqgJhMIqH89Q"></script>
<script src="/web/js/map.js"></script>
<script src="/web/js/infobox.js"></script>


@endsection