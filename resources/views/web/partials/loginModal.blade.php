<div class="modal fade" id="login_2" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
            <form  action="{{ route('login') }}" method="POST" class="popup-form" id="myLogin">
                @csrf
                <div class="login_icon"><i class="icon_lock_alt"></i></div>
                <input type="email" name="email" class="form-control form-white" required placeholder="Email">
                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                <input type="password" name="password" class="form-control form-white" required placeholder="Contraseña">
                <div class="text-left">
                    <a href="/password/reset">Olvidaste la contraseña?</a>
                </div>
                <div class="text-right">
                    <a href="#" data-toggle="modal" data-target="#register" data-dismiss="modal">Registrate</a>
                </div>
                <button type="submit" class="btn btn-submit">Iniciar sesion</button>
            </form>
        </div>
    </div>
</div><!-- End modal -->   