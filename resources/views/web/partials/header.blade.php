<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col--md-4 col-sm-4 col-xs-4">
                <a href="/" id="logo">
                    <img src="/web/img/logoF.png" alt="" data-retina="true" class="hidden-xs" style="width: 280px;">
                    <img src="/web/img/logoF.png" alt="" data-retina="true" class="hidden-lg hidden-md hidden-sm" style="    width: 170px;margin-top: -7px;margin-left: -10px;">
                </a>
            </div>
            <nav class="col--md-8 col-sm-8 col-xs-8">
            <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
            <div class="main-menu">
                <div id="header_menu">
                    <img src="/web/img/logoF.png" width="190"  alt="" data-retina="true">
                </div>
                <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>
                <ul>
                    <li class="submenu">
                    <a href="/">Inicio</a>
                    </li>
                    <li class="submenu">
                    <a href="javascript:void(0);" class="show-submenu">Acerca de Nosotros<i class="icon-down-open-mini"></i></a>
                    <ul>
                        <li><a href="/acerca_nosotros">Acerca de Nosotros</a></li>
                        <li><a href="/terminos_y_condiciones">Termino y Condiciones</a></li>
                    </ul>
                    </li>
                    <li><a href="/registro_restaurante">Vende Aca</a></li>
                    <li><a href="/registro_repartidor">Trabaja Aca</a></li>
                    @if (!$user=Auth::user())
                        <li><a href="#0" data-toggle="modal" data-target="#login_2">Login</a></li>
                    @else
                        <li><a href="/perfil">Perfil</a></li>
                        <li><a href="/logOut">Cerrar Sesion</a></li>
                    @endif
                </ul>
            </div><!-- End main-menu -->
            </nav>
        </div><!-- End row -->
    </div><!-- End container -->
    </header>