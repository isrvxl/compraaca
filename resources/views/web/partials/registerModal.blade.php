<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myRegister" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
            <form action="{{ route('register') }}" method="POST" class="popup-form" id="myRegister">
                @csrf
                <div class="login_icon"><i class="icon_lock_alt"></i></div>
                <input type="text" name="f_name" class="form-control form-white" required placeholder="Nombre">
                <input type="text" name="l_name" class="form-control form-white" required placeholder="Apellido">
                <input type="text" name="rut" oninput="checkRut(this)" required class="form-control form-white" placeholder="Rut">
                <input type="text" name="phone" class="form-control form-white" required placeholder="Telefono">
                <input type="email" name="email" class="form-control form-white" required placeholder="Email">
                <input type="password" name="password" class="form-control form-white" required placeholder="Contraseña"  id="password1">
                <input type="password" name="password_confirmation" required class="form-control form-white" placeholder="Confirmar Contraseña"  id="password2">
                <div id="pass-info" class="clearfix"></div>
                <div class="checkbox-holder text-left">
                    <div class="checkbox">
                        <input type="checkbox" value="accept_2" id="check_2" name="check_2" />
                        <label for="check_2"><span>I Agree to the <strong>Terms &amp; Conditions</strong></span></label>
                    </div>
                </div>
                <button type="submit" class="btn btn-submit">Registrarse</button>
            </form>
        </div>
    </div>
</div><!-- End Register modal -->

@section('scripts')
 <script src="/js/validarRUT.js"></script>
@endsection