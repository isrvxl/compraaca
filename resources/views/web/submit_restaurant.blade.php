@extends('layouts.web')

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Vende con nosotros</h1>
         <p>Comienza a incrementar tus ventas y clientes</p>
         <p></p>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
    <div class="main_title margin_mobile">
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
			
            <h2 class="nomargin_top">Aumenta tus clientes</h2>
            <p>
                Con nuestra alianza ganamos todos.
        </div>
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
			<div class="feature">
				<i class="icon_datareport"></i>
				<h3><span>Aumenta</span> tus ventas</h3>
				<p>
					Al realizar alianza de tu marca con nosotros, promocionaremos tu negocio por nuestras redes, lo cual te ayudará a incrementar tus ventas.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.2s">
			<div class="feature">
				<i class="icon_bag_alt"></i>
				<h3><span>Promociona</span> tus productos</h3>
				<p>
					Constantemente realizamos promociones para tus productos y marca generando campañas publicitarias, dándole más visibilidad, público y venta a tu negocio.
				</p>
			</div>
		</div>
	</div><!-- End row -->
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.3s">
			<div class="feature">
				<i class="icon_chat_alt"></i>
				<h3><span>Soporte</span> telefónico y WSP</h3>
				<p>
					Nuestras ejecutivas estarán siempre disponibles por este medio, para responder a tus solicitudes ya sea de tu local, promociones, pagos, con el fin de brindarte la solución más acorde a tus necesidades.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.3s">
			<div class="feature">
				<i class="icon_mobile"></i>
				<h3><span>Entrega</span> Eficiente</h3>
				<p>
					Nuestro objetivo es que la entrega de tu pedido sea de manera rápida y segura, además de la recepción conforme del cliente que le entregamos en su puerta todo lo solicitado.
				</p>
			</div>
		</div>
	</div><!-- End row -->
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.4s">
			<div class="feature">
				<i class="icon_wallet"></i>
				<h3><span>Recibe Pago</span> en Efectivo</h3>
				<p>
					Nosotros tenemos todas las modalidades de pago, sin que tu corras ningún risgo de estafa o fraude. Nosotros te aseguramos el pago y tomamos los riegos.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.5s">
			<div class="feature">
				<i class="icon_creditcard"></i>
				<h3><span>Pago seguro</span> en el dia</h3>
				<p>
					Nosotros te depositaremos el costo del pedido en línea máximo dentro de las 12 hrs efectuado el pedido, dependiendo de la modalidad y el canal de compra.
				</p>
			</div>
		</div>
	</div><!-- End row -->
</div><!-- End container -->


	<div class="container text-center">
		<img src="/web/images/bannerestaurant.jpg" style="width: 100%;" alt="">
    </div><!-- End container -->

<div class="container margin_60">
	    <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Por favor llene el formulario a continuación</h2>
            <p>
				Llena los campos y nosotros te contactaremos
            </p>
        </div>

	<div class="row">
    	<div class="col-md-8 col-md-offset-2">
            <form action="/restorantRequestMail" method="POST">
                @csrf
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Nombre:</label>
									<input type="text" class="form-control" required name="f_name">
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Apellido:</label>
									<input type="text" class="form-control" required name="l_name">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Email:</label>
									<input type="email" name="email" required class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Teléfono:</label>
									<input type="text" name="phone" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nombre del Local:</label>
                                   <input type="text" name="restaurant_name" required class="form-control">
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group">
									<label>Rut Comercio:</label>
                                   <input type="text" name="restaurant_rut" required class="form-control">
								</div>
							</div>
						</div><!-- End row  -->
                        <div class="row">
							<div class="col-md-6">
								<div class="form-group">
                                	<label>Giro:</label>
                                   <input type="text" name="giro" required class="form-control">
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group">
                                	<label>Direccion:</label>
                                   <input type="text" name="address" required class="form-control">
								</div>
							</div>
						</div><!-- End row  -->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
                                	<label>Comuna:</label>
                                   <input type="text" name="comuna" required class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                	<label>Ciudad:</label>
                                   <input type="text" name="restaurant_city" required class="form-control">
								</div>
							</div>
						</div><!-- End row  -->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Codigo Postal:</label>
									<input type="text" name="restaurant_postal_code" required class="form-control">
								</div>
							</div>
						</div><!-- End row  -->
                      
                        <div id="pass-info" class="clearfix"></div>
                        <div class="row">
                        	<div class="col-md-6">
								<!-- # TODO hacer modal que muestre terminos y condiciones -->
									<label><input name="mobile" type="checkbox" value="" class="icheck" checked>Acepta <a href="#0">terminos y condiciones</a>.</label>
							</div>
                            </div><!-- End row  -->
                        <hr style="border-color:#ddd;">
                        <div class="text-center"><button type="submit" class="btn_full_outline">Enviar</button></div>
			</form>
        </div><!-- End col  -->
    </div><!-- End row  -->
</div><!-- End container  -->
<!-- End Content =============================================== -->
    
@endsection