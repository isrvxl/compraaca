@extends('layouts.web')

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Detalles de Venta</h1>
         <p></p>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->
<div class="col-lg-12 mt">
    <div class="row content-panel">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="invoice-body">
                <div class="pull-left">
                    <h1>{{$sale->store->name}}</h1>
                    <address>
                        <strong>{{$sale->store->email}}</strong><br>
                        {{$sale->store->address}} <br>
                        <abbr title="Phone">P:</abbr>{{$sale->store->phone}}
                    </address>
                </div>
                <div class="pull-right">
                    <h2>VENTA</h2>
                </div>
                <div class="clearfix"></div>
                    <br>
                    <br>
                    <br>
                <div class="row">
                    <div class="col-md-9">
                        <h4>{{$sale->user->f_name}} {{$sale->user->l_name}}</h4>
                        <address>
                            <strong>{{$sale->user->email}}</strong><br>
                            {{$sale->user->rut}}<br>
                            {{$sale->details->delivery_address}}<br>
                            <abbr title="Phone">P:</abbr> {{$sale->user->phone}}
                        </address>
                    </div>
                                    <!-- /col-md-9 -->
                    <div class="col-md-3">
                        <br>
                        <div>
                            <div class="pull-left"> VENTA NO : </div>
                            <div class="pull-right"> {{$sale->id}} </div>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                                                <!-- /col-md-3 -->
                            <div class="pull-left"> FECHA DE VENTA : </div>
                            <div class="pull-right"> {{date('d-m-Y', strtotime($sale->updated_at))}} </div>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                    </div>
                                    <!-- /invoice-body -->
                </div>
                                <!-- /col-lg-10 -->
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width:60px" class="text-center">QTY</th>
                            <th class="text-left">PRODUCTO</th>
                            <th style="width:140px" class="text-right">PRECIO UNIT</th>
                            <th style="width:90px" class="text-right">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sale->saleitems as $item)
                            
                            <tr>
                                <td class="text-center">{{$item->qty}}</td>
                                <td>{{$item->product->name}}</td>
                                <td class="text-right">$ {{$item->product->price}}</td>
                                <td class="text-right">$ {{$item->amount}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2" rowspan="4">
                                  <h4>Observaciones</h4>
                                <p>{{$sale->details->description}}</p>
                                  <td class="text-right"><strong>Subtotal</strong></td>
                                <td class="text-right">$ {{$sale->amount}}</td>
                          </tr>
                    
                        <tr>
                            <td class="text-right no-border"><strong>Delivery</strong></td>
                            <td class="text-right">$ {{$sale->delivery_fee}}</td>
                        </tr>
                        <tr>
                            <td class="text-right no-border">
                                <div><strong>Total</strong></div>
                            </td>
                            <td class="text-right"><strong >$ {{$sale->amount + $sale->delivery_fee}}</strong></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    
<script type="text/javascript" language="javascript" src="/lib/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/lib/advanced-datatable/js/DT_bootstrap.js"></script>

@endsection