@extends('layouts.web')

@section('css')


	
@endsection

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window"  id="short"  data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
            <h1>Haga su Pedido</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong>Sus Datos</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart.html" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step active">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong>Pago</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#0" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step disabled">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Terminado!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_3.html" class="bs-wizard-dot"></a>
                </div>  
		</div><!-- End bs-wizard --> 
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->
    
<!-- Content ================================================== -->
<div class="container margin_60_35">
		<div class="row">
			<div class="col-md-3">
				<div class="box_style_2 hidden-xs info">
					<h4 class="nomargin_top">Detalles Usuario<i class="icon-list-alt pull-right"></i></h4>
						<ul>
							<li><p>Nombre: {{$user->f_name}}</p></li>
							<li><p>Apellido: {{$user->l_name}}</p></li>
							<li><p>Telefono: {{$user->phone}}</p></li>
							<li><p>Email: {{$user->email}}</p></li>
							<li><p>Rut: {{$user->rut}}</p></li>
						</ul>
				</div><!-- End box_style_2 -->
                
				<div class="box_style_2 hidden-xs" id="help">
					<i class="icon_lifesaver"></i>
					<h4>Necesitas <span>Ayuda?</span></h4>
					<a href="tel://004542344599" class="phone">+569-8898-5377</a>
					<h5>Horario</h5>
					<small> 10:00 - 22:00</small>
				</div>
			</div><!-- End col-md-3 -->
            
			<div class="col-md-6">
				<div class="box_style_2">
					<h2 class="inner">Metodos de Pago</h2>
					<div class="payment_select nomargin">
						<label><input type="radio" value="" name="payment_method" class="icheck">Pago con Flow</label>
						<i class="icon_creditcard"></i>
					</div>
				</div><!-- End box_style_1 -->
			</div><!-- End col-md-6 -->
            
			<div class="col-md-3" id="sidebar">
            	<div class="theiaStickySidebar">
					<div id="cart_box" >
						<h3>Tu orden<i class="icon_cart_alt pull-right"></i></h3>
						<table class="table table_summary">
						<tbody id="cartTable">
							@foreach ($sale->saleitems as $item)
							<tr>
								<td>
								 <strong>{{$item->qty}}</strong> {{$item->product->name}}
								</td>
								<td>
									<strong class="pull-right">$ {{ $item->amount }}</strong>
								</td>
							</tr>
							@endforeach
						</tbody>
						</table>
						<hr>
						<table class="table table_summary">
						<tbody>
						<tr>
							<td>
								Subtotal <span id="subTotal" class="pull-right">$ {{ $sale->amount }}</span>
							</td>
						</tr>
						<tr>
							<td>
								Delivery <span class="pull-right">$ {{ $sale->delivery_fee }}</span>
							</td>
						</tr>
						<tr>
							<td>
								<strong>Total <span class="pull-right">$ {{ $sale->amount + $sale->delivery_fee }}</span></strong> 
							</td>
						</tr>
						</tbody>
						</table>
						<hr>
						<a href="/sale/{{$sale->id}}/pay" class="btn_full" >Pagar</a>
						
					</div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
			</div><!-- End col-md-3 -->
            
		</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->
    
@endsection

@section('scripts')
    <!-- SPECIFIC SCRIPTS -->
<script src="/js/ResizeSensor.min.js"></script>
<script src="/js/theia-sticky-sidebar.min.js"></script>
<script>
    jQuery('#sidebar').theiaStickySidebar({
      additionalMarginTop: 80
    });
</script>

<script src="/js/sales.js"></script>
@endsection