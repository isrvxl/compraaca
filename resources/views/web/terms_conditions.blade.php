@extends('layouts.web')

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Terminos y Condiciones</h1>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
		<div class="col-md-12">
			<h4>1 - GENERAL</h4> 
				<p>
					COMPRAACA y sus sociedades vinculadas prestan servicios a los usuarios, según los siguientes términos y condiciones (los “Términos y Condiciones”).
					Al utilizar cualquier servicio actual o futuro de COMPRAACA se estará sujeto a los lineamientos y condiciones aplicables a tal Servicio o negocio.
				</p>
				<p>	
					Cualquier Usuario que desee acceder y/o usar el Portal o los Servicios podrá hacerlo sujetándose a estos Términos y Condiciones, junto con todas las demás políticas y principios que rigen en COMPRAACA y que son incorporados al presente por referencia o están disponibles en https://www.compraaca.com. 
					El desconocimiento del contenido de los Términos y Condiciones, no justifica el incumplimiento de los mismos, y mucho menos, faculta a los Usuarios para tomar medidas particulares o legales que desconozcan lo planteado en estos Términos y Condiciones.
				</p>
				<p>
					Con su registro y utilización del Portal, los Usuarios están manifestando su aceptación, expresa e inequívoca de los Términos y Condiciones. Respecto de menores de edad que quieran hacer uso del Portal, COMPRAACA se permite informar que deben contar con autorización de sus padres o representantes legales previa realización del registro. 
					COMPRAACA se reservan el derecho a negarse a prestar el servicio, cerrar cuentas o eliminar o editar contenido a su entera discreción.
				</p>
				<p>
					<ul>
						<li>CUALQUIER PERSONA QUE NO ACEPTE ESTOS TÉRMINOS Y CONDICIONES GENERALES Y/O CUALQUIERA DE LAS DEMÁS POLÍTICAS, TÉRMINOS Y CONDICIONES PARTICULARES Y PRINCIPIOS QUE RIGEN EN COMPRAACA, DEBERÁN ABSTENERSE DE UTILIZAR EL PORTAL Y/O LOS SERVICIOS.</li>
					</ul>
				</p>
				<p>
					Si los Usuarios tienen dudas respecto a estos Términos y Condiciones pueden comunicarse con nuestro equipo de Atención al Cliente a través del Whatsapp en nuestro Portal o al correo electrónico contacto@compraaca.com
				</p>
			<br>
			<h4>2 - PRIVACIDAD</h4> 
				<p>
					Por favor, revisar nuestra Política de Privacidad, disponible en https://www.compraaca.com/, que también rige la relación con COMPRAACA, a fin de entender nuestras prácticas.
				</p>
			<br>
			<h4>3 -  DEFINICIONES</h4>
				<p>
					<strong>"Aplicación"</strong>, hace referencia a la aplicación “COMPRAACA” disponible para las tecnologías móviles.
				</p>
				<p>
					<strong>"Bienes"</strong>, son los bienes, productos o servicios que pueden llegar a ser ofrecidos por los Oferentes a través del Portal e integran el Pedido del Usuario. Los Bienes son suministrados por los Oferentes, siendo responsables de los mismos ante los Usuarios.
				</p>
				<p>
					<strong>"Oferente"</strong>, se refiere a agentes externos y/o terceros ajenos a COMPRAACA, que previamente han contratado con COMPRAACA sus servicios de intermediación, aportando toda la información de los Bienes que se exhiben a través del Portal (precio, características, y en general todas sus condiciones objetivas). 
					Estos, en su calidad exclusiva de productor, proveedor y/o expendedor, son los directamente encargados de cumplir con todas las características objetivas del producto y/o servicio publicado en el Portal.
				</p>
				<p>
					<strong>"Pedido"</strong>, hace referencia a la solicitud del Usuario a través del Portal de los Bienes de Oferentes.
				</p>
				<p>
					<strong>"Nosotros", "Nuestro", y "COMPRAACA"</strong>, siempre que se haga referencia a los vocablos, se está haciendo referencia directa a la sociedad HOSTEL ADVENTURE SPA , RUT 76.604.625-8, domicilio en calle Carmen Grossi   N° 131,Santiago de Chile, representada por su representante legal don Arturo Aníbal López  Apablaza.
				</p>
				<p>
					<strong>"Servicio"</strong>, hace referencia al servicio de intermediación solicitado por el Usuario a través del Portal.
				</p>
				<p>
					<strong>"Sitio Web"</strong>, hace referencia al sitio web https://www.compraaca.com.
				</p>
				<p>
					<strong>"Usuario" y "Cliente"</strong>, hace referencia a todas las personas físicas o jurídicas que accedan a nuestro Portal y realizan Pedidos sea a través del Sitio Web o de la Aplicación.
				</p>
				<p>
					<strong>"Portal"</strong>, hace referencia a nuestro Sitio Web y/o Aplicación a través de las cuales, en calidad de intermediarios, facilitamos el contacto entre Oferentes, Usuarios y repartidores.	
				</p>
				<p>
					En caso de utilizarse alguna de estas palabras definidas en plural, tendrán el mismo significado que el indicado en el presente.	
				</p>
			<br>
			<h4>4 -  CAPACIDAD</h4>
				<p>
					Los Servicios y Productos solo están disponibles para Usuarios que tengan capacidad legal para contratar. No podrán utilizar los Servicios las personas que no tengan esa capacidad, los menores de edad sin autorización de su padre o tutor o Usuarios de COMPRAACA que hayan sido suspendidos temporalmente o inhabilitados definitivamente.
				</p>
				<p>
					Para registrar una empresa como Usuario, se deberá contar con la capacidad suficiente como para contratar a nombre y representación de la entidad como así también de obligar a la misma según estos Términos y Condiciones aquí previstos.
				</p>
				<p>
					En caso de desear adquirir bebidas alcohólicas y/o tabaco, en pleno cumplimiento de las normas aplicables, el Usuario deberá acreditar ser mayor de 18 años, exhibiendo una identificación al momento de la entrega. Asimismo, en el caso de venta de bebidas alcohólicas pueden existir restricciones horarias en función de la normativa del lugar de residencia del Usuario.
				</p>
			<br>
			<h4>5 - DERECHOS DE AUTOR</h4>
				<p>
					El contenido del Portal, incluyendo, pero sin limitarse a los textos, gráficas, imágenes, logotipos, íconos, software y cualquier otro material, -al cual en adelante se hará referencia como el “Material”, está protegido bajo las leyes aplicables de propiedad industrial y propiedad intelectual. Todo el Material es de propiedad de COMPRAACA o de sus proveedores. 
					Queda prohibido modificar, copiar, reutilizar, extraer, explotar, reproducir, comunicar al público, hacer segundas o posteriores publicaciones, cargar o descargar archivos, enviar por correo, transmitir, usar, tratar o distribuir de cualquier forma la totalidad o parte de los contenidos incluidos en el Portal. 
					El uso no autorizado del Material puede constituir una violación de las leyes sobre derechos de autor, leyes de propiedad industrial u otras leyes. Ningún Usuario podrá vender o modificar el Material de manera alguna, ni ejecutar o anunciar públicamente el Material, ni distribuirlo para propósitos comerciales. 
					Tampoco se permitirá copiar o adaptar el código HTML que COMPRAACA crea para generar su página web o plataforma, ya que el mismo está protegido por los derechos de autor. Todo uso no autorizado se presumirá como indebido y podrá ser sancionado por la ley.
				</p>
			<br>
			<h4>6 - MARCAS COMERCIALES</h4>
				<p>
					El logotipo de COMPRAACA, y otras marcas indicadas en el Portal son marcas comerciales de COMPRAACA. Otros gráficos, logotipos, encabezados de página, íconos de botones, guiones y nombres de servicio de COMPRAACA son marcas comerciales o imágenes comerciales de COMPRAACA. 
					Las marcas comerciales e imagen comercial COMPRAACA no podrán ser utilizadas en relación con cualquier producto o servicio que no sea de COMPRAACA, en su caso, de ninguna manera que pueda causar confusión entre los Usuarios o que desestime o desacredite a COMPRAACA.
				</p>
			<br>
			<h4>7 - USO AUTORIZADO DEL PORTAL</h4>
				<p>
					El Portal tiene como fin principal la intermediación entre Usuarios y Oferentes para realizar Pedidos en las ciudades de Chile que posean el servicio, facilitando las relaciones y transacciones entre Usuarios y Oferentes adheridos al Portal. 
					Asimismo, mediante el Portal los Usuarios podrán contratar Servicios, realizando Pedidos, según los términos y condiciones aplicables, publicados en el Portal. 
					COMPRAACA contacta al Oferente, redirecciona el pedido, se encarga de informar respecto de su disponibilidad o no al Usuario de conformidad con la información que le haya proporcionado el Oferente.
				</p>
				<p>
					A través del Portal se centralizan y otorgan a los Usuarios, todas las herramientas necesarias para que éste realice un Pedido.
					COMPRAACA  siempre hará su mejor esfuerzo para que una vez que el pedido haya sido confirmado por parte del Comercio al cual ha sido solicitado, el Comercio entregue el pedido de acuerdo a las especificaciones contratadas
				</p>
			<br>
			<h4>8 - CREACIÓN DE CUENTA</h4>
				<p>
					Para el uso del Portal, los Usuarios deberán crear una cuenta de usuario (la “Cuenta”) donde se le solicitarán ciertos datos tales como: nombre, fecha de nacimiento, dirección, teléfono, correo electrónico, documento de identidad, identificación fiscal, y datos para procesar los pagos online (los “Datos”). 
					Los Usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia, autenticidad y actualización de sus Datos. En ningún caso COMPRAACA  se responsabiliza por la certeza de los Datos de los Usuarios.
				</p>
				<p>
					Si los Usuarios tienen cuenta en Facebook, podrán crear su Cuenta con la utilización de las credenciales allí incluidas.
				</p>
				<p>
					COMPRAACA podrá requerir alguna información o documentación adicional a los efectos de comprobar o corroborar los Datos, pudiendo suspender temporal o definitivamente a aquellos Usuarios cuyos Datos no hayan podido ser confirmados.
				</p>
				<p>
					Para acceder a su Cuenta personal, el Usuario deberá ingresar su correo electrónico y una contraseña la cual deberá mantener siempre de manera confidencial. Si el Usuario olvida su contraseña, podrá restablecerla haciendo clic en “Olvidé mi contraseña”.
				</p>
				<p>
					La Cuenta es única e intransferible. Queda prohibido que un Usuario registre o tenga más de una Cuenta. De detectarse el incumplimiento a lo antes previsto, COMPRAACA se reserva el derecho de cancelar, suspender o inhabilitar las cuentas, sin perjuicio de otras medidas legales que pueda tomar.	
				</p>
				<p>
					COMPRAACA no puede garantizar la identidad de los Usuarios. El Usuario es responsable de todas las transacciones realizadas en su Cuenta, debiendo notificar a COMPRAACA inmediatamente, de forma fehaciente, cualquier uso no autorizado de la misma, así como cualquier sustracción, divulgación o pérdida de sus datos de acceso al Portal. 
					COMPRAACA vela por la protección de los datos de los Usuarios. Sin embargo, no será responsable del uso fraudulento que puedan hacer terceros de la Cuenta del Usuario, incluidos usos indebidos de sus datos asociados a los pagos online.
				</p>
				<p>
					COMPRAACA se reserva el derecho de rechazar cualquier solicitud de registro o de cancelar una registración previamente aceptada, sin que esté obligada a comunicar o exponer las razones de su decisión y sin que ello genere algún derecho a indemnización o resarcimiento.
				</p>
				<p>
					Los Usuarios tienen la facultad de ejercer el derecho de acceso, en cualquier momento y sin restricciones, de sus datos personales. COMPRAACA tiene la atribución de atender las denuncias y reclamos que se interpongan con relación al incumplimiento de las normas sobre protección de datos personales.
				</p>
			<br>
			<h4>9 - PROCEDIMIENTO DE PEDIDOS Y ACLARACIONES GENERALES</h4>
				<p>
					COMPRAACA  ofrece una plataforma de intermediación en línea (el Portal) para que los Oferentes puedan ofrecer sus Bienes, y los Usuarios puedan adquirirlos y solicitar su entrega a domicilio. 
					En ese marco, COMPRAACA exhibe la información de los Bienes del Oferente según la información provista por este último, no responsabilizándose por la exactitud y/o veracidad de la misma.
				</p>
				<p>
					El Usuario comprende y acepta que COMPRAACA no produce, provee, vende, expende ni es agente, distribuidor, ni en general ningún tipo de comercializador de los Bienes exhibidos; por lo anterior, la relación de compraventa es estructurada entre el Usuario y el Oferente. 
					Asimismo, el Usuario reconoce que es el único responsable por la correcta consignación de las direcciones de entrega y recogida de los Pedidos, eximiendo de responsabilidad a COMPRAACA y a los repartidores por cualquier error o negligencia. <br>
					El Usuario podrá ver las diferentes opciones disponibles sin estar registrado. Sin embargo, para poder finalizar el Pedido, el Usuario debe estar registrado con su Cuenta y debe ingresar el domicilio de entrega. 
					Una vez realizado lo anterior, el Usuario podrá ver las diferentes opciones de Oferentes, Bienes, ubicación de los Oferentes, etc.
				</p>
				<p>
					Cuando el Usuario ha definido a dónde y qué quiere pedir, debe ingresar al perfil del Oferente en el Portal y elegir el/los Bienes que desea adquirir. Una vez seleccionados, se pone a disposición del Usuario las características, condiciones y valor total del Pedido, según la información provista por el Oferente. 
					El Usuario deberá validar el Pedido y seleccionar el método de pago elegido a través de los medios de pago disponibles en COMPRAACA para cada Oferente, según se indica en la sección 12 de estos Términos y Condiciones.
				</p>
				<p>
					Seleccionada la forma de pago, el Usuario deberá confirmar el Pedido.
				</p>
				<p>
					Es importante aclarar que todo Usuario se compromete a pagar el precio del Pedido desde el momento en el que recibe la comunicación de confirmación del Pedido según se indica más abajo en estos Términos y Condiciones. 
					Tras la recepción de dicha comunicación, el Usuario únicamente podrá abstenerse de realizar el pago si el Pedido es cancelado de forma correcta, según lo previsto en estos Términos y Condiciones.	
				</p>
				<p>
					Si el Usuario no recibe el Pedido en el domicilio indicado para ello y/o si surge algún contratiempo en donde no se verifiquen los datos del Usuario y se rechace el pedido una vez fue confirmado por el Portal y en ese sentido no se reciban correcciones una vez efectuada la confirmación, 
					toda la responsabilidad recaerá sobre el Usuario y éste deberá indemnizar al Portal haciéndose cargo de todos los costos que generó el error en la transacción, a modo de ejemplo, el costo de envío que pudiera aplicar.	
				</p>
				<p>
					Todos los Pedidos que se realizan a través del Portal son transmitidos a los Oferentes, quienes podrán contactarse con el Usuario, a modo de ejemplo, si los productos o servicios que integran el Pedido no se encuentran disponibles.
				</p>
				<p>
					Cuando el Oferente acepta o rechaza el Pedido, se comunica al Usuario con un correo electrónico, una notificación PUSH (emergente) u otro medio de comunicación, en donde se rechaza o confirma la recepción del Pedido, el cual se produce de forma automática con los detalles.	
				</p>
				<p>
					En caso de rechazarse el Pedido por parte del Oferente, COMPRAACA notificará al Usuario sin la obligación de notificar los motivos del rechazo. <br>
					En caso de confirmarse el pedido, la PUSH (emergente) u otro medio de comunicación, indicará el tiempo de entrega del Pedido. Dicho tiempo de entrega es exclusivamente estimado, y el Usuario reconoce que el mismo podrá sufrir pequeños ajustes mientras el Pedido se prepara (dichos ajustes se verán reflejados en el estado del pedido que se visualiza en el Portal). 
					El Usuario, al hacer su Pedido, afirma conocer y aceptar que innumerables factores como el tráfico, el clima, los horarios pico y la capacidad de preparación del pedido y entrega de algunos Oferentes, pueden ser limitantes para asegurar la hora de entrega.
				</p>
				<p>
					Durante el tiempo que transcurra entre la confirmación del Pedido y la efectiva entrega del mismo, el Usuario podrá comunicarse en cualquier momento con COMPRAACA a efectos de hacer preguntas, presentar quejas, entre otros, casos en los cuales siempre recibirá una respuesta en el menor tiempo posible. 
					El Usuario podrá cancelar el Pedido una vez transcurrido el plazo de entrega indicado en la confirmación del pedido.
				</p>
				<p>
					COMPRAACA  siempre actuará como intermediaria y centrará sus esfuerzos en resolver todas las quejas o situaciones problemáticas que se configuren con ocasión a demoras, pedidos incompletos o equivocados, etc. En todos los casos, sin excepción, las quejas deben ser presentadas en un lenguaje decente y respetuoso, atendiendo a los presupuestos mínimos de cortesía y educación. 
					En caso contrario, el Portal no estará obligado a proporcionar respuesta alguna, y por el contrario, de acuerdo con su propio criterio, podrá proceder a bloquear al Usuario de su base de datos, quedando dicho Usuario imposibilitado para utilizar los Servicios nuevamente.
				</p>
				<p>
					Todos los comentarios y calificaciones son revisados por el personal de COMPRAACA y son debidamente registrados en el historial de cada Usuario según la guía de comentarios disponible en https://www.compraaca.com.
				</p>
				<p>
					La entrega del Pedido podrá realizarse con repartidores del Oferente u otros puestos a disposición por COMPRAACA, según se indica en el Portal. El Usuario comprende y acepta que COMPRAACA no se hará responsable ante el Usuario por la falta de entrega del Pedido o en caso que se viera alterado o perjudicado – en caso que el Oferente se encargue de este servicio.
				</p>
			<br>
			<h4>10 - PRECIO DE LOS BIENES</h4>
				<p>
					El precio de los Bienes será presentado en el Portal, según la información provista por el Oferente, antes de que la transacción sea aceptada por el Usuario. Dicho precio incluirá todos los posibles recargos por impuestos, adiciones, etc., que sean aplicables a cada transacción. Se aclara que algunos Oferentes pueden tener importe mínimo de pedido y que también pueden aplicar costos de envío, lo que se indicará en los perfiles de los Oferentes.
				</p>
				<p>
					El Usuario comprende y acepta que, en caso de solicitar modificaciones a los Bienes integrantes del Pedido, el precio podrá tener modificaciones.
				</p>
				<p>
					Es responsabilidad del Usuario asegurarse en el momento de la entrega del Pedido, que éste corresponde con los Bienes solicitados y que todo lo recibido sea correcto. En caso contrario, el Usuario puede rechazar el Pedido devolviéndoselo al repartidor.
				</p>
			<br>
			<h4>11 - MEDIOS DE PAGO</h4>
				<p>
					Los métodos de pago para cada Pedido dependen de cada Oferente, por lo que el Usuario podrá verificar esta información en sus respectivos perfiles.
				</p>
				<p>
					1.	Pago en la entrega
				</p>
				<p>
					<ul>
						1.	POS: El Usuario podrá abonar el Pedido con POS en el domicilio indicado para le entrega. <br>
						2.	Pago en efectivo: El Usuario podrá abonar el Pedido al repartidor en efectivo en moneda de curso legal en el país. Asimismo, el Usuario podrá indicar el monto exacto con el que abonará el Pedido, de manera que pueda establecerse si existe algún cargo en diferencia que deba retornarse al Usuario.
					</ul>
				</p>
				<p>
					2.	Pago Online
				</p>
				<p>
					<ul>
						El Usuario podrá abonar el precio del Pedido mediante ejemplo, tarjeta de crédito, débito, etc.).
						Cuando el Usuario elija realizar el pago online: (i) deberá cargar una tarjeta de crédito, débito o medios de pago electrónico para abonar el monto del Pedido. Esta información es gestionada de forma segura; sus datos no son almacenados en COMPRAACA, 
						deberá seleccionar un medio de pago electrónico cargado previamente en el Portal.
					</ul>
				</p>
				<p>
					El Usuario deberá tener particular cuidado en el suministro de datos personales, los cuales deben ser completos y verídicos en el momento de realización del Pedido. 
					Asimismo, al ingresar los datos el Usuario garantiza que los datos que suministra de su medio de pago electrónico son de su propiedad y tiene suficientes fondos para hacer el pago, cuando éste sea el método de preferencia;  
					su identidad corresponde con la información contenida en la identificación oficial y original otorgada por las autoridades nacionales.
				</p>
				<p>
					El monto se descuenta al realizar el Pedido. Si posteriormente el Pedido es cancelado -ya sea por COMPRAACA, por el Usuario o por el Oferente-, COMPRAACA procesará la devolución al instante. 
					Sin embargo, el reintegro depende de los tiempos estipulados en las políticas del emisor de cada medio de pago electrónico (por ej. de la tarjeta), sobre los cuales COMPRAACA no tiene ninguna responsabilidad ni capacidad de modificar los tiempos que éstas devoluciones le insuman, 
					resultando suficiente medio de prueba del actuar diligente de COMPRAACA y por lo tanto, exonerándolo de responsabilidad, la acreditación a través de documentación de procesamiento de pagos, de la solicitud de devolución de las sumas por COMPRAACA a las empresas de procesamiento de pagos y empresas emisoras de tarjetas, según corresponda. <br>
					Si las devoluciones mencionadas no pueden ejecutarse por factores asociados a las emisoras de los medios de pago electrónicos, el importe será acreditado mediante un Cupón en la cuenta del Usuario y se le notificará sobre dicho crédito.	
				</p>
				<p>
					Las transacciones online podrán ser rechazadas, cuando la validación y aceptación del pago no sea confirmada o aceptada por COMPRAACA o cuando la entidad bancaria del Usuario o el medio de pago así lo determinen COMPRAACA, 
					no se hace responsable por los trámites internos de autorización que disponga la entidad bancaria/financiera que emita los instrumentos de pago, ni por lo permisos que requieren los medios de pago para efectuar compras por internet.
				</p>
				<p>
					En el detalle de la transacción el Usuario podrá verificar la información completa de pago. Si el Usuario abonó el pedido con Pago Online y el pago fue confirmado, no deberá realizar otro pago por el Pedido, salvo que hubiera realizado modificaciones al Pedido según se indica en estos Términos y Condiciones.		
				</p>
				<p>
					Cuando el Pedido se abone electrónicamente, el Usuario podrá (dependiendo de la zona de entrega) destinar un monto o porcentaje extra en concepto de propina al repartidor que entregue su Pedido.
				</p>
				<p>
					COMPRAACA  no designa ninguna parte de su pago como propina o gratificación al repartidor. Cualquier manifestación por parte de COMPRAACA en el sentido de que dar una propina es “voluntario” y/o “no requerido” en los pagos que realiza para los Servicios o Bienes, 
					no pretende sugerir que COMPRAACA proporciona importes adicionales al repartidor. El Usuario es libre de proporcionar un pago adicional como gratificación al repartidor que entregue su Pedido.	
				</p>
				<p>
					En caso de cancelarse el pedido -ya sea por COMPRAACA, por el Usuario o por el Oferente-, la devolución de lo entregado por el Usuario en concepto de propina, operará de igual forma que la devolución del importe del Pedido, según se indica más arriba.
				</p>
			<br>
			<h4>12 - PUBLICIDAD</h4>
				<p>
					COMPRAACA  cuenta con un servicio de publicidad por medio del cual ésta se hace llegar a los Usuarios a través de banderas (banners), correos electrónicos y/u otros medios. 
					Los enlaces o vínculos que dirigen a otros sitios web de propiedad de terceras personas se suministran para su conveniencia únicamente y COMPRAACA no respalda, recomienda o asume responsabilidad alguna sobre el contenido de estos. <br>
					El Usuario puede solicitar no recibir más correos electrónicos u otras notificaciones relativas a publicidad mediante la configuración del perfil de su cuenta.
				</p>
			<br>
			<h4>13 - PROMOCIONES, CONCURSOS Y EVENTOS</h4>
				<p>Las promociones, concursos, descuentos, sorteos y eventos que se implementen a través del Portal estarán sujetas a las reglas y condiciones que en cada oportunidad se establezcan, de manera anticipada por COMPRAACA. 
					Bajo ningún motivo esto implica que COMPRAACA está obligada a realizar algún tipo de sorteo, simplemente regula una situación con anterioridad, en caso de que COMPRAACA de manera voluntaria, decida llevar a cabo tales actividades. 
					En caso de que se realice alguna de las mencionadas actividades será requisito mínimo para acceder a tales oportunidades o beneficios comerciales, 
					que el Usuario se encuentre debidamente registrado en el Portal y cumpla con las condiciones de mayoría de edad o cuente con la autorización de su padre o tutor. Los términos y condiciones de promociones, 
					incentivos y/o actividades especiales con una vigencia limitada se publicarán en debida forma en el Portal y se entienden como parte de estos Términos y Condiciones.
				</p>
				<p>
					El Usuario acepta que COMPRAACA podrá realizar acuerdos comerciales con terceros a los efectos de realizar comunicaciones promocionales, incluyendo el envío de muestras gratuitas a domicilio junto al Pedido.
				</p>
			<br>
			<h4>14 – RESPONSABILIDAD</h4>
				<p>
					COMPRAACA únicamente pone a disposición de los Usuarios un espacio virtual de intermediación que les permite ponerse en comunicación mediante internet a los Usuarios, los Oferentes y los repartidores y así comprar y vender, respectivamente, los Bienes. 
					COMPRAACA no es proveedora ni propietaria de los Bienes, no tiene posesión de ellos ni los ofrece en venta. El Oferente es el único responsable de la existencia, calidad, cantidad, estado, integridad, inocuidad o legitimidad de los Bienes. El Usuario conoce y acepta que al realizar Pedidos a los Oferentes lo hace bajo su propio riesgo.
				</p>
				<p>
					Si los Bienes no se ajustan a la calidad, idoneidad o seguridad propias y necesarias, el Usuario podrá solicitar el cambio de estos, o la devolución, según corresponda; y siempre que haya fundamento jurídico y racional para ello, para lo cual COMPRAACA trasladará tal solicitud al Oferente, 
					que decidirá la procedencia o no de ésta en su calidad de productor y único responsable respecto de las características objetivas ofertadas. La garantía solo aplica para características objetivas de los productos.
				</p>
			<br>
			<h4>15 - REGLAS GENERALES</h4>
				<p>
					Los Usuarios no pueden usar el Portal con el fin de transmitir, distribuir, almacenar o destruir material  en violación de cualquier ley aplicable o regulación, de manera que se infrinjan las leyes sobre derechos de autor, propiedad industrial, 
					secretos comerciales o cualquier otro derecho de propiedad intelectual de terceros o de manera que viole la privacidad, publicidad u otros derechos personales de terceros, o  en forma que sea difamatoria, obscena, amenazante o abusiva. 
					Esto sin perjuicio de normas particulares sobre la materia que sean imperativas en cada uno de los ordenamientos jurídicos correspondientes a los territorios en los cuales COMPRAACA prestará su servicio.	
				</p>
			<br>
			<h4>16 - REGLAS DE SEGURIDAD</h4>
				<p>
					A los Usuarios les está prohibido violar o intentar violar la seguridad del Portal. Específicamente los Usuarios, a modo de ejemplo y sin que implique limitación, no podrán  acceder a información que no esté dirigida o autorizada a dicho Usuario o acceder a servidores o cuentas a los cuales el Usuario no está autorizado a acceder;  
					intentar probar la vulnerabilidad de un sistema o red sin la debida autorización o violar las medidas de seguridad o autenticación;  intentar interferir con los servicios prestados a un Usuario, servidor o red, incluyendo pero sin limitarse al envío de virus a través del Portal o sobre carga de tráfico para denegación del servicio;  enviar correo electrónico no solicitado, 
					incluyendo promociones y/o publicidad de productos o servicios. La violación de cualquier sistema o red de seguridad puede resultar en responsabilidades civiles y penales. COMPRAACA investigará la ocurrencia de hechos que puedan constituir violaciones a lo anterior y cooperará con cualquier autoridad competente en la persecución de los Usuarios que estén envueltos en tales violaciones; 
					suplantar la identidad de otros usuarios o de personas naturales o jurídicas de cualquier índole; proporcionar información de identidad incorrecta, incompleta o falsa; y  bajo ninguna circunstancia se tolerará la acción de hacer pedidos falsos o crear usuarios con fines fraudulentos, actuar que será denunciado y estará sujeto a las prosecuciones legales aplicables.
				</p>
				<p>
					El Portal puede ser usado únicamente para propósitos legales. Se prohíbe su uso en cualquiera de las siguientes formas:	
				</p>
				<p>
					<ul>
						<li>Incluir en el Portal cualquier derecho de franquicia, esquema de pirámide, membrecía a un club o grupo, representación de ventas, agencia comercial o cualquier oportunidad de negocios que requiera un pago anticipado o pagos periódicos, solicitando el reclutamiento de otros miembros, sub-distribuidores o sub-agentes.</li>
						<li>Borrar o revisar cualquier material incluido en el Portal por cualquier otra persona o entidad, sin la debida autorización.</li>
						<li>Usar cualquier elemento, diseño, software o rutina para interferir o intentar interferir con el funcionamiento adecuado del Portal o cualquier actividad que sea llevada a cabo en el Portal.</li>
						<li>Intentar descifrar, compilar o desensamblar cualquier software comprendido en el Portal o que de cualquier manera haga parte de este.</li>
						<li>Como ya se mencionó, está terminantemente prohibido incluir en el Portal información falsa, inexacta, incompleta, incorrecta o engañosa.</li>
					</ul>
				</p>
			<br>
			<h4>17 - PROHIBICIÓN DE REVENTA, CESIÓN O USO COMERCIAL NO AUTORIZADO</h4>
				<p>
					Los Usuarios acuerdan no revender o ceder sus derechos u obligaciones al aceptar estos Términos y Condiciones. También se comprometen a no hacer un uso comercial no autorizado del Portal.
				</p>
			<br>
			<h4>18 - TERMINACIÓN</h4>
				<p>
					COMPRAACA se reserva el derecho, a su exclusiva discreción, de suspender o cancelar la registración del Usuario, y por lo tanto, negarle acceso a este Portal, ante el incumplimiento de estos Términos y Condiciones por parte de los Usuarios o ante la imposibilidad de verificar o autenticar cualquier información que estos hayan presentado en el registro para acceder al Portal. 
					Sin perjuicio, de conservar determinada información para efectos únicamente estadísticos y sin que ello implique bajo ningún entendido la preservación de datos personales, ya que se garantiza que tal información se conservará en términos de absoluto anonimato.
				</p>
			<br>
			<h4>19 - INFORMACIÓN ADICIONAL</h4>
				<p>
					COMPRAACA NO GARANTIZA QUE EL PORTAL OPERE LIBRE DE ERRORES O QUE SU SERVIDOR SE ENCUENTRE LIBRE DE VIRUS DE COMPUTADORES U OTROS MECANISMOS DAÑINOS. SI EL USO DEL PORTAL O DEL MATERIAL RESULTA EN LA NECESIDAD DE SOLICITAR SERVICIO DE REPARACIÓN O MANTENIMIENTO A SUS EQUIPOS O INFORMACIÓN O DE REEMPLAZAR SUS EQUIPOS O INFORMACIÓN, PEDIDOSYA NO ES RESPONSABLE POR LOS COSTOS QUE ELLO IMPLIQUE.
				</p>
				<p>
					EL PORTAL Y EL MATERIAL SE PONEN A DISPOSICIÓN DE LOS USUARIOS EN EL ESTADO EN QUE SE ENCUENTREN. NO SE OTORGA GARANTÍA ALGUNA SOBRE LA EXACTITUD, CONFIABILIDAD U OPORTUNIDAD DEL MATERIAL, LOS SERVICIOS, LOS TEXTOS, EL SOFTWARE, LAS GRÁFICAS Y LOS ENLACES O VÍNCULOS.
				</p>
				<p>
					EN NINGÚN CASO, COMPRAACA, SUS PROVEEDORES O CUALQUIER PERSONA MENCIONADA EN EL PORTAL, SERÁ RESPONSABLE POR DAÑOS DE CUALQUIER NATURALEZA, RESULTANTES DEL USO O LA IMPOSIBILIDAD DE USARLOS.
				</p>
			<br>
			<h4>20 - LEY APLICABLE</h4>
				<p>
					Al visitar el Portal, el Usuario acepta que las leyes de Chile, independientemente de los principios de conflicto de leyes, regirán estos Términos y Condiciones, así como cualquier controversia, de cualquier tipo, que pudiera surgir entre el Usuario y COMPRAACA.
				</p>
			<br>
			<h4>21 - POLÍTICAS, MODIFICACIÓN Y DIVISIBILIDAD DEL SITIO WEB</h4>
				<p>
					Favor de revisar nuestras otras políticas publicadas en el Sitio Web. Estas políticas también regirán su visita a COMPRAACA. Nos reservamos el derecho a hacer cambios a nuestro Portal, políticas y a estos Términos y Condiciones en cualquier momento. En caso de que alguna de estas condiciones resulte inválida, 
					nula o por cualquier razón inaplicable, tal condición se considerará separable y no afectará la validez y aplicabilidad de ninguna de las demás condiciones.
				</p>
			<br>
			<h4>ESPECIALES</h4>
				<p>
					a) “Descuento Diario o Semanal” realizado por comercios
				</p>
				<p>
					Descuento aplicable a los comercios adheridos a COMPRAACA, para compras realizadas en productos ofrecidos a través del Portal. Válida únicamente para Oferentes en cuya oferta en el Portal se indique el descuento. Mediante esta promoción, los Usuarios podrán comprar productos seleccionados de y por Oferentes, 
					y al hacerlo obtendrán el/los producto/s a un precio bonificado (el "Beneficio"), según se indique en la oferta del Oferente en el Portal. 
					Beneficio no acumulable con “Promociones” hechas por los Oferentes. Y “Promociones con Pago Online”. La acumulación será en el orden preindicado siempre que alguno de dichos beneficios esté disponibles. 
					El Beneficio aplicará automáticamente previo a finalizar la compra. Esta promoción estará vigente mientras se indique en la oferta del Oferente.	
				</p>
				<p>
					Aplican Términos y Condiciones generales.	
				</p>
		</div>
	</div><!-- End row -->
</div><!-- End container -->

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 nopadding features-intro-img">
			<div class="features-bg">
				<div class="features-img">
				</div>
			</div>
		</div>
	</div>
</div><!-- End container-fluid  -->
<!-- End Content =============================================== -->
    
@endsection