@extends('layouts.web')

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Haga su Pedido</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong>Sus Datos</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart.html" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong>Pago</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_2.html" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Terminado!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#0" class="bs-wizard-dot"></a>
                </div>  
		</div><!-- End bs-wizard --> 
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="box_style_2">
				<h2 class="innerRejected">Orden Rechazada!</h2>
				<div id="rejected">
					<i class="icon_close_alt2"></i>
					<h3>Vuelve a Intentarlo!</h3>
					<p>
						<div class="box_style_2 hidden-xs info">
							<h4 class="nomargin_top">Detalles Usuario<i class="icon-list-alt pull-right"></i></h4>
								<ul>
									<li><p>Nombre: {{$sale->user->f_name}}</p></li>
									<li><p>Apellido: {{$sale->user->l_name}}</p></li>
									<li><p>Telefono: {{$sale->user->phone}}</p></li>
									<li><p>Email: {{$sale->user->email}}</p></li>
									<li><p>Rut: {{$sale->user->rut}}</p></li>
								</ul>
						</div><!-- End box_style_2 -->
						<div class="box_style_2 hidden-xs info">
							<h4 class="nomargin_top">Detalles Tienda<i class="icon_cart_alt pull-right"></i></h4>
								<ul>
									<li><p>Nombre: {{$sale->store->name}}</p></li>
									<li><p>Direccion: {{$sale->store->address}}</p></li>
									<li><p>Telefono: {{$sale->store->phone}}</p></li>
									<li><p>Horario: {{$sale->store->hours}}</p></li>
								</ul>
						</div><!-- End box_style_2 -->
					</p>
				</div>
				<h4>Resumen</h4>
				<table class="table table-striped nomargin">
				<tbody>
					@foreach ($sale->saleitems as $item)
					<tr>
						<td>
						 <strong>{{$item->qty}}</strong> {{$item->product->name}}
						</td>
						<td>
							<strong class="pull-right">$ {{ $item->amount }}</strong>
						</td>
					</tr>
					@endforeach
				
				<tr class="sr-only">
					<td>
                        Horario de entrega <a href="#" class="tooltip-1" data-placement="top" title="" data-original-title="Please consider 30 minutes of margin for the delivery!"><i class="icon_question_alt"></i></a>
					</td>
					<td>
						<strong class="pull-right">Today 07.30 pm</strong>
					</td>
				</tr>
				<tr>
					<td class="total_confirm">
						 TOTAL
					</td>
					<td class="total_confirm">
						<span class="pull-right">$ {{ $sale->amount }}</span>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
		</div>
	</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->

@endsection
