@extends('layouts.web')

@section('css')

<!-- Bootstrap core CSS -->
<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--external css-->
<link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!-- Custom styles for this template -->
<link href="/css/style.css" rel="stylesheet">
<link href="/css/style-responsive.css" rel="stylesheet">
    
@endsection

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Bienvenido {{$user->name}}</h1>
         <p></p>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

   
<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">   
        <div class="panel-heading">
            <ul class="nav nav-tabs nav-justified">
                <li class="active">
                    <a data-toggle="tab" href="#edit">Editar Perfil</a>
                </li>
                <li >
                    <a data-toggle="tab" href="#miscompras">Mis Compras</a>
                </li>
                
            </ul>
        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
	    @endif

        <div class="panel-body">
            <div class="tab-content">
                <div id="miscompras" class="tab-pane active">
                    <table class="table table-striped table-advance table-hover">
                        <h4><i class="fa fa-angle-right"></i> Mis Compras</h4>
                        <hr>
                        <thead>
                          <tr>
                            <th><i class="fa fa-shopping-cart"></i> Pedido</th>
                            <th><i class="fa fa-calendar"></i> Fecha</th>
                            <th><i class="fa fa-store"></i> Tienda</th>
                            <th class="hidden-phone"><i class="fa fa-money"></i> Precio</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($user->sales as $s)
                          <tr>
                            <td>
                                {{$s->id}}
                            </td>
                            <td>
                                {{$s->created_at->toDateString()}}
                            </td>
                            <td>
                                {{$s->store->name}}
                            </td>
                            <td>
                                {{$s->amount}}
                            </td>
                            <td data-id="{{$s->id}}">   
                                <a class="btn btn-default" href="/mySales/{id}" style="background:transparent;padding: 0 12px" data-toggle="tooltip" data-placement="top" title="Mas info">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                          </tr>
                        @endforeach  
                        </tbody>
                    </table>    
                </div> 
                <div id="edit"  class="tab-pane">
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Editar Perfil</h4>
                        <form action="{{ route('users.update', $user->id) }}" class="form-horizontal style-form" method="POST">  
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="f_name" class="form-control" value="{{$user->f_name}}"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Apellido</label>
                                <div class="col-sm-10">
                                    <input type="text" name="l_name" class="form-control" value="{{$user->l_name}}">
                                </div>
                            </div>    
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" value="{{$user->email}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Rut</label>
                                <div class="col-sm-10">
                                    <input type="text" name="rut" oninput="checkRut(this)" class="form-control" value="{{$user->rut}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Telefono</label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone" class="form-control" value="{{$user->phone}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Direccion</label>
                                <div class="col-sm-10">
                                    <input type="text" name="address" class="form-control" value="{{$user->address}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Ciudad</label>
                                <div class="col-sm-10">
                                    <input type="text" name="city" class="form-control" value="{{$user->city}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                <input type="submit" value="Actualizar" class="btn btn-primary pull-right"  style="width:200px">
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.sales.info')		
    
@endsection

@section('scripts')
<script src="/lib/jquery/jquery.min.js"></script>
<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="/lib/jquery.dcjqaccordion.2.7.js"></script>
<script src="/lib/jquery.scrollTo.min.js"></script>
<script src="/lib/jquery.nicescroll.js" type="text/javascript"></script>
<script src="/js/validarRUT.js"></script>

<script>
$(function(){
    $('body').on('click','.info',function(){
	var id = $(this).parent().data('id')
		$.get('/sales/'+id, function(r){
			$('#infoModal form').attr('action', "/sales/"+r.id)
			$('#infoModal input[name=nameUser]').val(r.user.f_name + r.user.l_name)
			$('#infoModal input[name=emailUser]').val(r.user.email)
			$('#infoModal input[name=phoneUser]').val(r.user.phone)
			$('#infoModal input[name=rutUser]').val(r.user.rut)
			$('#infoModal input[name=addressUser]').val(r.user.address)
			$('#infoModal input[name=nameStore]').val(r.store.name)
			$('#infoModal input[name=emailStore]').val(r.store.email)
			$('#infoModal input[name=addressStore]').val(r.store.address)
			$('#infoModal input[name=phoneStore]').val(r.store.phone)
			$('#infoModal input[name=hoursStore]').val(r.store.hours)
			$('#infoModal input[name=totalSale]').val(r.amount)
			$('#infoModal').modal()
		})
	})
		$("body").on('click', '.cancelEdit', function(e){
			e.preventDefault()
			location.reload()
	})
})
</script>
@endsection