@extends('layouts.web')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" data-parallax="scroll" data-image-src="/images_store/{{$stores->img_backgroud}}" data-natural-width="1400" data-natural-height="470">
    <div id="subheader">
	<div id="sub_content">
    	<div id="thumb"><img src="/images_store/{{$stores->img}}" alt=""></div>
                    <div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i> (<small><a href="detail_page_2.html">Read 98 reviews</a></small>)</div>
                    <h1>{{ $stores->name }}</h1>
                    <div><em>{{$stores->type_label}}</em></div>
                    <div><i class="icon_pin"></i> {{$stores->address}}
    </div><!-- End sub_content -->
</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
		<div class="row">
        <input type="hidden" name="store_id" id="storeId" value="{{$stores->id}}">
			<div class="col-md-3">
            	<p><a href="javascript:history.back()" class="btn_side">Volver a Buscar</a></p>
				<div class="box_style_1">
					<ul id="cat_nav">
						@foreach ($typeList as $t)
						<li><a href="#m{{$t['id']}}" class="active">{{$t['name']}}</a></li>
						@endforeach
					</ul>
				</div><!-- End box_style_1 -->
                
				<div class="box_style_2 hidden-xs" id="help">
					<i class="icon_lifesaver"></i>
					<h4>Necesitas <span>Ayuda?</span></h4>
					<a href="tel://56988985377" class="phone">+569-8898-5377</a>
					<h5>Horario</h5>
					<small> 10:00 - 22:00</small>
				</div>
			</div><!-- End col-md-3 -->
            
			<div class="col-md-6">
				<h2 class="inner">Menu</h2>
				@foreach ($typeList as $t)
				<div class="box_style_2" id="main_menu">
					<h3 class="nomargin_top" id="m{{$t['id']}}">{{$t['name']}}</h3>
					<table class="table table-striped cart-list">
					<thead>
					<tr>
						<th>
							 Item
						</th>
						<th>
							 Precio
						</th>
						<th>
							 Orden
						</th>
					</tr>
					</thead>
					<tbody>
						@foreach ($t['products'] as $p)
							<tr>
								<td>
									<a data-fancybox="gallery" href="/images_product/{{$p->img}}">
										<figure class="thumb_menu_list">
											<img style="width: 100px" src="/images_product/{{$p->img}}">
										</figure>
										
									</a>
									<h5>{{$p->name}}</h5>
									<p style="    max-width: 320px;">
										{{$p->description}}
									</p>
								</td>
								<td>
									<strong>$ {{number_format ( $p->price, 0, ',', '.' ) }}</strong>
								</td>
								<td class="options">
									<div class="">
									<a class="goToCart" data-object="{{$p}}" aria-expanded="true">
											<i class="icon_plus_alt2"></i>
										</a>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
					</table>
					
				</div><!-- End box_style_1 -->
				@endforeach
			</div><!-- End col-md-6 -->
            
			<div class="col-md-3" id="sidebar">
            <div class="theiaStickySidebar">
				<div id="cart_box" >
					<h3>Tu orden<i class="icon_cart_alt pull-right"></i></h3>
					<table class="table table_summary">
					<tbody id="cartTable">
					
					</tbody>
					</table>
					<hr>
					<table class="table table_summary">
					<tbody>
					<tr>
						<td>
							 Subtotal <span id="subTotal" class="pull-right"></span>
						</td>
					</tr>
					</tbody>
					</table>
					<hr>
					<a class="btn_full goToSale" data-store="{{$stores['id']}}" >Ordenar Ahora</a>
				</div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
			</div><!-- End col-md-3 -->
            
		</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->

@endsection

@section('scripts')

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<!-- SPECIFIC SCRIPTS -->
<script  src="/web/js/cat_nav_mobile.js"></script>
<script>$('#cat_nav').mobileMenu();</script>
<script src="/web/js/ResizeSensor.min.js"></script>
<script src="/web/js/theia-sticky-sidebar.min.js"></script>
<script>
    jQuery('#sidebar').theiaStickySidebar({
      additionalMarginTop: 80
    });
</script>
<!-- SMOOTH SCROLL -->
<script>
	$('#cat_nav a[href^="#"]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
			|| location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			   if (target.length) {
				 $('html,body').animate({
					 scrollTop: target.offset().top -75
				}, 800);
				return false;
			}
		}
	});
</script>

<script src="/js/sales.js"></script>
@endsection