@extends('layouts.web')

@section('content')
       <!-- SubHeader =============================================== -->
       <section class="header-video">
        <div id="hero_video">
            <div id="sub_content">            
                <h1 class="hidden-xs">El Mas Completo Delivery de tu Comuna</h1>
                <p>
                    Encuentra en tu zona lo que Buscas.
                </p>
                <form method="post" action="/tiendas" style="margin-top: 240px;">
                    @csrf
                    <div id="custom-search-input">
                        <div class="input-group">
                            <input type="hidden" id="lat" name="lat">
                            <input type="hidden" id="lng" name="lng">
                            <input type="text" id="pac-input" name="address" class=" search-query" placeholder="Tu Direccion o Comuna">
                            <span class="input-group-btn">
                            <input type="submit" class="btn_search" value="submit">
                            </span>
                        </div>
                    </div>
                </form>
            </div><!-- End sub_content -->
        </div>
        <img src="/web/img/video_fix.png" alt="" class="header-video--media" data-video-src="" data-teaser-source="/web/video/intro" data-provider="" data-video-width="1920" data-video-height="960">
        <div id="count" class="hidden-xs" >
            <ul>
                @foreach ($categories as $c)
                    <li><a style="color: white" href="/tiendas/categoria/{{$c->id}}">{{$c->name}}</a></li>
                @endforeach
            </ul>
        </div>
        </section><!-- End Header video -->
        <!-- End SubHeader ============================================ -->
        
       <!-- Content ================================================== -->
             <div class="container margin_60">
            
             <div class="main_title">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
			    @endif
                <h2 class="nomargin_top" style="padding-top:0">Como Funciona</h2>
                <p>
                    DIFERENTES PRODUCTOS EN TU COMUNA
                </p>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="box_home" id="one">
                        <span>1</span>
                        <h3>Busca Por Tu direccion</h3>
                        <p>
                            Encuentra los servicios mas cercanos
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box_home" id="two">
                        <span>2</span>
                        <h3>Escoge tus compras</h3>
                        <p>
                            Tenemos de todo tipo de servicios
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box_home" id="three">
                        <span>3</span>
                        <h3>Paga Online o Efectivo</h3>
                        <p>
                            Es rapido y totalmente Seguro
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box_home" id="four">
                        <span>4</span>
                        <h3>Delivery</h3>
                        <p>
                            Espera nuestro Delivery a la Puerta de Tu Casa
                        </p>
                    </div>
                </div>
            </div><!-- End row -->
            
           
            </div><!-- End container -->
                
        <div class="white_bg">
        <div class="container margin_60">
            <div class="main_title">
                <h2 class="nomargin_top">Tiendas</h2>
            </div>
            <div class="row">
                    @foreach ($stores as $s)
                    <div class="col-md-6">
                        <a href="/details/{{$s->id}}" class="strip_list">
                            <div class="ribbon_1">Destacado</div>
                            <div class="desc">
                                <div class="thumb_strip">
                                    <img src="/images_store/{{$s->img}}" alt="">
                                </div>
                                <div class="rating">
                                    <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                                </div>
                                <h3>{{$s->name}}</h3>
                                <div class="type">
                                    {{$s->tipo ? $s->tipo->name : 'Indefinido'}}
                                </div>
                                <div class="location">
                                    {{$s->address}} <br><span class="opening">Horario: {{$s->hours}}</span>
                                </div>
                                <ul>
                                    <li>Delivery<i class="icon_check_alt2 ok"></i></li>
                                </ul>
                            </div><!-- End desc-->
                        </a><!-- End strip_list-->
                    </div><!-- End col-md-6-->
                    @endforeach
            </div><!-- End row -->   
            
        </div><!-- End container -->
    </div><!-- End white_bg -->
    <section class="parallax-window" data-parallax="scroll" data-image-src="/web/img/banner2.png" data-natural-width="1200" data-natural-height="550">
        <div class="parallax-content"></div><!-- End subheader -->
    </section><!-- End section -->
    <div class="container margin_60">
          <div class="main_title margin_mobile">
                <h2 class="nomargin_top">Trabaja Con Nosotros</h2>
            </div>
              <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <a class="box_work" href="/registro_restaurante">
                    <img src="/web/images/workus.png" width="848" height="480" alt="" class="img-responsive">
                    <h3>Registra ahora tu <br> negocio<span>Empieza a ganar clientes</span></h3>
                    <p class="text-justify">Publicitamos y distribuimos tu marca, entregando el servicio
                        de venta y entrega de tus productos tal cual lo hicieras tu,
                        pero sin el costo de operaciones asociados. Además
                        logramos mayor cobertura llegado a toda tu comuna y
                        provincia y en consecuencia aumentamos tus ventas.</p>
                    <div class="btn_1">Contactanos</div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="box_work" href="/registro_repartidor">
                    <img src="/web/images/delivery.png" width="848" height="480" alt="" class="img-responsive">
                    <h3>Estamos buscando Repartidores<span>Empieza a Ganar Dinero</span></h3>
                    <p class="text-justify">Te inscribes, te registramos y comienzas a trabajar en tu
                        sector. Nosotros ofrecemos y distribuimos los negocios
                        locales en tu comuna por lo que tus trayectos serán cerca de
                        tu hogar. Ten dinero diariamente en tu bolsillo. Tu decides
                        tus horas y días de trabajo!!.</p>
                    <div class="btn_1">Contactanos</div>
                    </a>
                </div>
          </div><!-- End row -->
          </div><!-- End container -->
    
@include('web.contact')

@endsection

@section('scripts')
<script src="/js/map-web.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSY5xPLXVGwJ96DaIOn1CMqgJhMIqH89Q&libraries=places&callback=initMap" async defer></script>

    <script>
        $(document).ready(function() {
            'use strict';
            HeaderVideo.init({
            container: $('.header-video'),
            header: $('.header-video--media'),
            videoTrigger: $("#video-trigger"),
            autoPlayVideo: true
            });    

        });
    </script>
@endsection