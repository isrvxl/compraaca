@extends('layouts.web')

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="/web/images/banner2.png" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Acerca de Nosotros</h1>
         <p>Conoce nuestras metas y plan de integración al comercio comunal.</p>
         <p></p>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
		<div class="col-md-12 text-justify">
			<h3 class="nomargin_top">Algunas cosas sobre nosotros</h3>
			<p>Compra acá Spa nace en abril del 2020, prestando el servicio de transporte y delivery en todo el territorio nacional.
			</p>
			<p>
				Nuestro plan es ir generando distintas alianzas en cada localidad con los mejores locales comerciales y mercados de tu zona, para llevarte la gran variedad de productos y de la
				mejor calidad que se venden en tu comuna, con entrega en el momento, con los mismos valores como si fueras directo al local pero sin que te muevas de tu hogar. Tu solo pagas el
				servicio de delivery!!.
			</p>
			<h4>Nuestra Misión</h4>
			<p>
				Ofrecer a nuestros clientes los mejores productos y servicios de su mercado local logrando una relación de confianza y trasparencia con nuestros colaboradores, aliados y clientes,
				brindando un excelente servicio basado en la puntualidad y respeto de nuestro equipo humano y así entregar a nuestros clientes el mejor servicio de Delivery y pago de cuentas para
				hacer tu vida más simple y que disfrutes de tu tiempo.
			</p>
			<h4>Nuestra Visión</h4>
			<p>
				Consolidarnos cómo el mejor y más completo servicio de delivery de tu comuna a través de la satisfacción de nuestros clientes. Llegar a lugares remotos con el mejor del mercado
				local, contribuyendo al incremento del empleo. Ser reconocidos por clientes, proveedores, empleados como la mejor opción.
			</p>
		</div>
	
	</div><!-- End row -->
	<hr class="more_margin">
    <div class="main_title">
            <h2 class="nomargin_top">Por que comprar con nosotros?</h2>
            <p>
                Te llevamos lo mejor de tu comuna a la puerta de tu casa y acá tenemos muchas razones de porque preferirnos.
            </p>
        </div>
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
			<div class="feature">
				<i class="icon_building"></i>
				<h3><span>Los mejores</span> restaurantes</h3>
				<p>
					La mayor variedad de restaurantes y locales de tu comuna, diferentes opciones, según tus gustos y necesidades.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.2s">
			<div class="feature">
				<i class="icon_documents_alt"></i>
				<h3><span>+1000 tipos</span> de comidas</h3>
				<p>
					Menús variados, donde podrás elegir a tu gusto y conveniencia lo que quieres recibir en la puerta de tu casa.
				</p>
			</div>
		</div>
	</div><!-- End row -->
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.3s">
			<div class="feature">
				<i class="icon_bag_alt"></i>
				<h3><span>Envío o</span> para llevar</h3>
				<p>
					Tienes dos maneras de comprar en tus locales preferidos, delivery donde pagas tu pedido mas el valor de despacho y para llevar donde tu mismo retiras desde el local el pedido y solo pagas este.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.4s">
			<div class="feature">
				<i class="icon_mobile"></i>
				<h3><span>Soporte</span> en linea</h3>
				<p>
					Tenemos soporte en linea de 10am a 10pm todos los dias del año. Llamanos o contactate via whatsapp y te ayudamos a efectuar tus compras y solucionar tus dudas.
				</p>
			</div>
		</div>
	</div><!-- End row -->
	<div class="row">
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.5s">
			<div class="feature">
				<i class="icon_wallet"></i>
				<h3><span>Recibo</span> de efectivo</h3>
				<p>
					Si deseas pagar en efectivo, debes registrarte para poder comprar y tu pago debe ser justo, ya que nuestros repartidores no portan dinero para dar vuelto.
				</p>
			</div>
		</div>
		<div class="col-md-6 wow fadeIn" data-wow-delay="0.6s">
			<div class="feature">
				<i class="icon_creditcard"></i>
				<h3><span>Pago seguro</span> con tarjeta</h3>
				<p>
					Ocupamos sitema Flow, otra empresa nacional emprendedora donde podras ocupar cualquier tarjeta de creito o debito, onepay, webpay, servipag, mach y multicaja.
				</p>
			</div>
		</div>
	</div><!-- End row -->
</div><!-- End container -->

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 nopadding features-intro-img">
			<div class="features-bg">
				<div class="features-img">
					
				</div>
			</div>
		</div>
		<div class="col-md-6 nopadding">
			<div class="features-content">
				<h3>“YO ME QUEDO DISFRUTANDO”</h3>
				<p>
					Descarga la App y haz los pedidos desde tu casa, tu oficina, tu carrete o de donde estes!!
				</p>
				<p>
					Compras en los mejores locales de tu barrio, comida, cervezas, licores, carnes, carbon, pizzas, sushi, abarrotes, farmacia y todo lo que tu comuna vende nosotros te lo llevamos.
				</p>
				<p>
					Ya no te muevas de donde estas, sigue disfrutando y compartiendo sin ponerte en risego tu ni a los demas, dejanos eso a nosotros!!
				</p>
			</div>
		</div>
	</div>
</div><!-- End container-fluid  -->
<!-- End Content =============================================== -->
    
@endsection