@extends('layouts.app')

@section('content')
	<form class="form-login" method="POST" action="{{ route('login') }}">
		@csrf
		<h2 class="form-login-heading">Administrador</h2>
		<div class="login-wrap">
			<label for="">Correo</label>
			<input type="email" class="form-control" placeholder="Email" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
			@error('email')
				<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
				</span>
			@enderror
			<br>
			<label for="">Contraseña</label>
			<input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
			@error('password')
				<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
				</span>
			@enderror
			<br>
			<span class="pull-right">
				<a data-toggle="modal" href="/password/reset"> Olvidaste tu Contraseña?</a>
			</span>
			
			<button class="btn btn-theme btn-block" type="submit" style="margin-top: 30px"><i class="fa fa-lock"></i> Ingresar</button>
			<hr>
		</div>
	</form>

@endsection
