<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="pizza, delivery food, fast food, sushi, take away, chinese, italian food">
        <meta name="description" content="">
        <meta name="author" content="Ansonika">
        <title>Compra Aca - Delivery</title>

        <!-- Favicons-->
        <link rel="shortcut icon" href="/web/img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/web/img/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/web/img/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/web/img/apple-touch-icon-144x144-precomposed.png">
        
        <!-- GOOGLE WEB FONT -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

        <!-- BASE CSS -->
        <link href="/web/css/animate.min.css" rel="stylesheet">
        <link href="/web/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/css/menu.css" rel="stylesheet">
        <link href="/web/css/style.css" rel="stylesheet">
        <link href="/web/css/responsive.css" rel="stylesheet">
        <link href="/web/css/elegant_font/elegant_font.min.css" rel="stylesheet">
        <link href="/web/css/fontello/css/fontello.min.css" rel="stylesheet">
        <link href="/web/css/magnific-popup.css" rel="stylesheet">
        <link href="/web/css/pop_up.css" rel="stylesheet">
        
        <!-- YOUR CUSTOM CSS -->
        <link href="/web/css/custom.css" rel="stylesheet">
        
        <!-- Modernizr -->
        <script src="/web/js/modernizr.js"></script> 
        @yield('css')
    </head>

    <body>

        <div id="preloader">
            <div class="sk-spinner sk-spinner-wave" id="status">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div><!-- End Preload -->
        @include('web.partials.header')
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning alert-dismissible" role="alert" style="    position: fixed;right: 0;z-index: 99999;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{$error}}
                </div> 
            @endforeach
        @endif
        @yield('content')
        @include('web.partials.footer')
        <div class="layer"></div><!-- Mobile menu overlay mask -->
        @include('web.partials.loginModal')
        @include('web.partials.registerModal')
    <!-- COMMON SCRIPTS -->
    <script src="/web/js/jquery-2.2.4.min.js"></script>
    <script src="/web/js/common_scripts_min.js"></script>
    <script src="/web/js/functions.js"></script>
    <script src="/web/assets/validate.js"></script>
    <script src="/js/jquery.serialize-object.min.js"></script>
    <!-- SPECIFIC SCRIPTS -->
    <script src="/web/js/video_header.js"></script>
    @yield('scripts')
    </body>
</html>