<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>CompraAca - Admin</title>

	<!-- Favicons -->
	<link rel="shortcut icon" href="/web/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/web/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/web/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/web/img/apple-touch-icon-144x144-precomposed.png">

	<!-- Bootstrap core CSS -->
	<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!--external css-->
	<link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="/css/zabuto_calendar.css">
	<link rel="stylesheet" type="text/css" href="/lib/gritter/css/jquery.gritter.css" />
	<!-- Custom styles for this template -->
	<link href="/css/style.css" rel="stylesheet">
	<link href="/css/style-responsive.css" rel="stylesheet">
	<script src="/lib/chart-master/Chart.js"></script>


    @yield('css')
</head>

<body >
  	<section id="container">
    	@include('admin.partials.topbar')
    
    	@include('admin.partials.sidebars')

		<section id="main-content">
			<section class="wrapper">
				@yield('content')
			</section>
		</section>

	  </section>
	  
    <script src="/lib/jquery/jquery.js"></script>

    <script src="/lib/bootstrap/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/lib/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/lib/jquery.scrollTo.min.js"></script>
    <script src="/lib/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/lib/jquery.sparkline.js"></script>
    <!--common script for all pages-->
    <script src="/lib/common-scripts.js"></script>
    <script type="text/javascript" src="/lib/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="/lib/gritter-conf.js"></script>
    <!--script for this page-->
    <script src="/lib/sparkline-chart.js"></script>
    <script src="/lib/zabuto_calendar.js"></script>
	@yield('scripts')
	<script>
		$(function(){
			$('[data-toggle="tooltip"]').tooltip()

		})
	</script>
    
</body>
</html>
