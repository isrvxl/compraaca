<!DOCTYPE html>
<html lang="en">

<head>


	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Dashboard">
	<meta name="keywords" content="pizza, delivery food, fast food, sushi, take away, chinese, italian food">
	<title>Compra Aca - Login</title>

	<!-- The core Firebase JS SDK is always required and must be listed first -->
	<script src="https://www.gstatic.com/firebasejs/7.19.1/firebase-app.js"></script>

	<!-- TODO: Add SDKs for Firebase products that you want to use
	https://firebase.google.com/docs/web/setup#available-libraries -->
	<script src="https://www.gstatic.com/firebasejs/7.19.1/firebase-analytics.js"></script>

	<script>
	// Your web app's Firebase configuration
	var firebaseConfig = {
		apiKey: "AIzaSyDI8s5D0emKFTLDWpatqElnlNnltBKwJMU",
		authDomain: "compraaca-12eed.firebaseapp.com",
		databaseURL: "https://compraaca-12eed.firebaseio.com",
		projectId: "compraaca-12eed",
		storageBucket: "compraaca-12eed.appspot.com",
		messagingSenderId: "634837933175",
		appId: "1:634837933175:web:0c2784a0ee82ec7ec9fa72",
		measurementId: "G-KHG3LT3E5W"
	};
	// Initialize Firebase
	firebase.initializeApp(firebaseConfig);
	firebase.analytics();

	// Retrieve Firebase Messaging object.
	const messaging = firebase.messaging();
	messaging.requestPermission().then(function() {
	console.log('Notification permission granted.');
	// TODO(developer): Retrieve an Instance ID token for use with FCM.
	// ...
	}).catch(function(err) {
	console.log('Unable to get permission to notify.', err);
	});
	
	// Get Instance ID token. Initially this makes a network call, once retrieved
	// subsequent calls to getToken will return from cache.
	messaging.getToken().then(function(currentToken) {
	if (currentToken) {
		console.log(currentToken);
		updateUIForPushEnabled(currentToken);
	} else {
		// Show permission request.
		console.log('No Instance ID token available. Request permission to generate one.');
		// Show permission UI.
		updateUIForPushPermissionRequired();
		setTokenSentToServer(false);
	}
	}).catch(function(err) {
	console.log('An error occurred while retrieving token. ', err);
	showToken('Error retrieving Instance ID token. ', err);
	setTokenSentToServer(false);
	});
	function setTokenSentToServer(sent) {
		window.localStorage.setItem('sentToServer', sent ? '1' : '0');
	}

	</script>

	<link rel="manifest" href="{{request()->root()}}/public/manifest.json">

	<!-- Favicons -->
	<link rel="shortcut icon" href="/web/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/web/img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/web/img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/web/img/apple-touch-icon-144x144-precomposed.png">

	<!-- Bootstrap core CSS -->
	<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!--external css-->
	<link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<!-- Custom styles for this template -->
	<link href="/css/style.css" rel="stylesheet">
	<link href="/css/style-responsive.css" rel="stylesheet">
</head>

<body>
	<div id="login-page">

		<div class="container">

			@yield('content')
			
		</div>

	</div>


		<!-- js placed at the end of the document so the pages load faster -->
	<script src="/lib/jquery/jquery.min.js"></script>
	<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
	<!--BACKSTRETCH-->
	<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
	<script type="text/javascript" src="/lib/jquery.backstretch.min.js"></script>
	<script>
		$.backstretch("img/login-bg.jpg", {
		speed: 500
		});
	</script>

</body>
</html>
