<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'f_name' => 'Aldo',
            'l_name' => 'Verdugo',
            'email' => 'aldo@gmail.com',
            'password' => Hash::make('secret'),
            'phone' => '+56988888888',
            'type' => 1
        ]);
    }
}
