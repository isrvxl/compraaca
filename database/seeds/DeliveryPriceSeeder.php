<?php

use Illuminate\Database\Seeder;

class DeliveryPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('delivery_prices')->insert([
            'point'     => 420,
            'amount'    => 285,
            'amount2'    => 340,
            'amount3'    => 600,
            'type'      => 1,
            'minimum'   => 1000,
        ]);

        DB::table('delivery_prices')->insert([
            'point'     => 500,
            'amount'    => 300,
            'amount2'    => 380,
            'amount3'    => 700,
            'type'      => 2,
            'minimum'   => 1200,
        ]);

        DB::table('delivery_prices')->insert([
            'point'     => 382,
            'amount'    => 259,
            'amount2'    => 309,
            'amount3'    => 545,
            'type'      => 3,
            'minimum'   => 909,
        ]);

        DB::table('delivery_prices')->insert([
            'point'     => 455,
            'amount'    => 273,
            'amount2'    => 345,
            'amount3'    => 636,
            'type'      => 4,
            'minimum'   => 1091,
        ]);

        
    }
}
