<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountsToDeliveryPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_prices', function (Blueprint $table) {
            $table->integer('amount2');
            $table->integer('amount3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_prices', function (Blueprint $table) {
            $table->integer('amount2');
            $table->integer('amount3');
        });
    }
}
