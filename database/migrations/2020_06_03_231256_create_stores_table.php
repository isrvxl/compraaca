<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('rut')->nullable();
            $table->string('phone');
            $table->string('hours');
			$table->string('address');
			$table->string('locality');
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lng',10,8)->nullable();
            $table->integer('type_id');
            $table->string('img');
            $table->longText('description');
            $table->integer('category_id');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
