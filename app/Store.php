<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = ['name', 'email', 'rut', 'phone', 'address', 'locality', 'city', 'lat', 'lng', 'type', 'img', 'category_id', 'status', 'description', 'hours', 'is_salient', 'img_backgroud', 'estimate_time'];

    const STATUS = [
        1 => 'Habilitada',
        2 => 'Deshabilitada',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function tipo()
    {
        return $this->belongsTo('App\Type', 'type');
    }
    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public static function dataForMap($stores) 
    {
        $data = [];
        foreach ($stores as $key => $s) {
            $temp = array(
                "name"                  => $s['name'],
                "location_latitude"     => $s['lat'],
                "location_longitude"    => $s['lng'],
                "name_point"            => $s['name'],
				"type_point"            => $s['tipo']['name'],
                "map_image_url"         => $s['img'],
                "description_point"     => $s['address'] .'<br><strong>Horario</strong>: '.$s['hours'],
                "url_point"             => 'details/'. $s['id']

            );
            array_push($data, $temp);
        }   
        return $data;
    }

    public static function dataForApp($stores, $latFrom, $lngFrom) 
    {
        $data = [];
        foreach ($stores as $key => $s) {
            $distance = Helper::distanceREE( $latFrom, $lngFrom, $s['lat'], $s['lng']);
            $temp = array(
                "id"            => $s['id'],
                "name"          => $s['name'],
                "distance"      => intval($distance) / 1000,
                "phone"         => $s['phone'],
                "hours"         => $s['hours'],
                "address"       => $s['address'],
                "locality"      => $s['locality'],
                "description"   => $s['description'],
                "lat"           => $s['lat'],
                "lng"           => $s['lng'],
                "type"          => $s['tipo']['name'],
                "category"      => $s['category']['name'],
                "img"           => $s['img'],
            );
            array_push($data, $temp);
        }   
        usort($data, fn($a, $b) => $a['distance'] - $b['distance']);

        foreach ($data as $key => $d) {
            $data[$key]['distance'] = number_format($d['distance'],1);
        }        
        return $data;
    }
    

}
