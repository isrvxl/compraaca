<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryPrice extends Model
{
    protected $fillable = ['amount','amount2','amount3', 'type', 'minimum' , 'point'];

    const TYPE= [
		1 => 'Tarifa Diurna cliente',
		2 => 'Tarifa Nocturna cliente',
		3 => 'Tarifa Diurna repartidor',
		4 => 'Tarifa Nocturna repartidor',
	];
}
