<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailForRestaurantContact extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contacto@compraaca.com', 'Restaurante')
        ->subject('Crear Restaurante')
        ->view('emails.contact_restaurant')
        ->with([
            'f_name' => $this->data['f_name'], 
            'l_name' => $this->data['l_name'], 
            'email' => $this->data['email'], 
            'phone' => $this->data['phone'], 
            'restaurant_name' => $this->data['restaurant_name'], 
            'restaurant_rut' => $this->data['restaurant_rut'], 
            'giro' => $this->data['giro'], 
            'address' => $this->data['address'], 
            'comuna' => $this->data['comuna'], 
            'restaurant_city' => $this->data['restaurant_city'], 
            'restaurant_postal_code' => $this->data['restaurant_postal_code'], 
        ]);
    }
}
