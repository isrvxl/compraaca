<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailForQueue extends Mailable
{
    use Queueable, SerializesModels;

    protected $f_name;
    protected $l_name;
    protected $email_order;
    protected $tel_order;
    protected $rut;
    protected $password;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($f_name, $l_name, $email_order, $tel_order, $rut, $password)
    {
        $this->f_name = $f_name;
        $this->l_name = $l_name;
        $this->email_order = $email_order;
        $this->tel_order = $tel_order;
        $this->rut = $rut;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@compraaca.com', 'Compra Aca')
                    ->subject('Bienvenido a Compra Aca')
                    ->view('emails.user_create')
                    ->with([
                        'f_name' => $this->f_name, 
                        'l_name' => $this->l_name, 
                        'email_order' => $this->email_order, 
                        'tel_order' => $this->tel_order, 
                        'rut' => $this->rut, 
                        'password' => $this->password 
                    ]);
    }
}
