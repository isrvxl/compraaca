<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailForDriverContact extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@compraaca.com', 'Web')
        ->subject('Quiero ser repartidor')
        ->view('emails.contact_driver')
        ->with([
            'f_name' => $this->data['f_name'], 
            'l_name' => $this->data['l_name'], 
            'email' => $this->data['email'], 
            'phone' => $this->data['phone'], 
            'vehiculo' => $this->data['vehiculo'], 
            'papeles' => $this->data['papeles'],
            'motor' => $this->data['motor'], 
            'bicicleta' => $this->data['bicicleta'], 
            'license' => $this->data['license'], 
            'mobile' => $this->data['mobile'], 
        ]);
    }
}
