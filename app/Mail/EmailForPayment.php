<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailForPayment extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contacto@compraaca.com', 'Ventas')
            ->subject('Nueva Venta')
            ->view('emails.infopayment')
            ->with([
                'nameUser' => $this->data->user['f_name'].' '.$this->data->user['l_name'], 
                'emailUser' => $this->data->user['email'], 
                'phonelUser' => $this->data->user['phone'],
                'addressUser' => $this->data->user['address'],
                'deliveryAddress' => $this->data->details['deliveryAddress'],
                'deliveryPrice' => $this->data['delivery_fee'],
                'rutUser' => $this->data->user['rut'],
                'totalSale' => $this->data['amount'],
                'statusSale' => $this->data['status'],
                'items' => $this->data->saleitems,
                'saleId' => $this->data['id'],
                'message' => $this
            ]);
    }
}
