<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailForContact extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contacto@compraaca.com', 'Contacto')
        ->subject('Necesidad Contacto')
        ->view('emails.contact')
        ->with([
            'name' => $this->data['name'], 
            'email' => $this->data['email'], 
            'subject' => $this->data['subject'], 
            'content' => $this->data['content'], 
        ]);
    }
}
