<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'rut' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'locality' => 'required',
            'city' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'type' => 'required',
            'img' => 'required',
            'category_id' => 'required',
            'status' => 'required',
        ];
    }
}
