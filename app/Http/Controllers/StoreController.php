<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStorePost;
use App\{Store,Category,Type,User,Sale,Product, Helper};
use Auth;

class StoreController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::orderBy('id','asc')->get();
        $categories = Category::where('type', 1)->get();
        $types = Type::where('type', 1)->get();
        $sale = Sale::orderBy('created_at','asc')->get();
        $status = Sale::STATUS;
        return view('admin.stores.list', ['menu' => 2, 'sale' => $sale, 'status' => $status, 'stores' => $stores, 'categories' => $categories, 'types' => $types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = 2;
        $categories = Category::where('type', 1)->get();
        $types = Type::where('type', 1)->get();
        return view('admin.stores.create', ['menu' => $menu, 'categories' => $categories, 'types' => $types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
    
        $data['is_salient'] = ( isset($data['is_salient']) && $data['is_salient'] == 'on') ? 1 : 0;
        
        $store = Store::create($data);

        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_store'), $filename);
			$store->img = $filename;
			$store->save();
        }

        if ($request->file('img_backgroud')) {
            $filename = time() . "." . $request->img_backgroud->extension();
            $request->img_backgroud->move(public_path('/images_store'), $filename);
			$store->img_backgroud = $filename;
			$store->save();
        }

     
        return back()->with('status', 'Tienda creada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        $categories = Category::where('type', 1)->get();
        $types = Type::where('type', 1)->get();
        return view('admin.stores.edit', ['menu' => 2, 'store' => $store, 'categories' => $categories, 'types' => $types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $data = $request->all();
        $data['is_salient'] = ( isset($data['is_salient']) && $data['is_salient'] == 'on') ? 1 : 0;

        $store->update($data);

        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_store'), $filename);
			$store->img = $filename;
			$store->save();
        }
        if ($request->file('img_backgroud')) {
            $filename = time() . "." . $request->img_backgroud->extension();
            $request->img_backgroud->move(public_path('images_store'), $filename);
			$store->img_backgroud = $filename;
			$store->save();
        }
        
        return back()->with('status', 'Tienda actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        $store->delete();
        return back()->with('status', 'Tienda eliminada con exito');
    }

    public function geoList(Request $request)
    {
        
        $stores = Store::where('status', 1)->get();
        foreach ($stores as $key => $s) {
            $distance = Helper::distanceREE($s['lat'], $s['lng'], $request->lat, $request->lng);
            $stores[$key]['distance'] = intval($distance) / 1000;
            $stores[$key]['type_label'] = $s->tipo ? $s->tipo->name : 'Indefinido';
        }
        $stores = $stores->toArray();
        usort($stores, fn($a, $b) => $a['distance'] - $b['distance']);

        $mapStore = Store::dataForMap($stores);
        $categories = Category::where('type', 1)->get();
        $types = Type::where('type', 1)->get();
        
        return view('web.list', [
            'mapStore'  => $mapStore,
            'stores'    => $stores, 
            'address'   =>$request->address,
            'categories'=> $categories, 
            'types'     => $types,
            'lat'       =>$request->lat,
            'lng'       =>$request->lng,
        ]);
    }

    public function categoryList($id)
    {
        $stores = Store::where('category_id', $id )->where('status', 1)->get();
       
        $mapStore = Store::dataForMap($stores);
       
        $categories = Category::where('type', 1)->get();
        $types = Type::where('type', 1)->get();
 
        return view('web.list', [
            'mapStore'  => $mapStore,
            'stores'    => $stores, 
            'categories'=> $categories, 
            'types'     => $types,
            'lat'       =>'',
            'lng'       =>'',
        ]);
    }

    public function typeList($id)
    {
        $stores = Store::where('type', $id)->where('status', 1)->get();
        $mapStore = Store::dataForMap($stores);
        $categories = Category::where('type', 1)->get();
        $types = Type::where('type', 1)->get();

        return view('web.list', [
            'mapStore'  => $mapStore,
            'stores'    => $stores, 
            'categories'=> $categories, 
            'types'     => $types,
            'lat'       =>'',
            'lng'       =>'',
        ]);
    }

    
    public function details($id)
    {
        $stores = Store::find($id);
        $typeList = [];
        foreach($stores->products as $key => $p ){
            if ($p->tipo) {
                array_push($typeList, array (
                    "id"    => $p->tipo->id,
                    "name"  => $p->tipo->name,
                ));
            }
        }
        $typeList = array_map("unserialize", array_unique( array_map("serialize", $typeList) ));

        foreach ($typeList as $key => $t) {
            $typeList[$key]['products'] = Product::where('store_id', $id)->where('type', $t['id'])->get();
        }

        return view('web.detail_page', ['stores' => $stores,  'typeList' => $typeList]);
    }

    public function orderCart($id)
    {
        $sale = Sale::where('id', $id)->first();
        //dd($sale->store);
        $user = Auth::user();
        return view('web.detail_cart', ['sale' => $sale, 'user' => $user]);
    }

   
}
