<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TypeController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.types.create', ['type'=> new type()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $type = Type::create($data);

        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_types'), $filename);
			$type->img = $filename;
			$type->save();
        }

        return back()->with('status', 'Tipo creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Type::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        return Type::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $data = $request->all();

        $type->update($data);

        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_types'), $filename);
			$type->img = $filename;
			$type->save();
        }


        return back()->with('status', 'Tipo actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $type->delete();
        return back()->with('status', 'Tipo eliminado con exito');
    }
}
