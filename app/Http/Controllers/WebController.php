<?php

namespace App\Http\Controllers;

use App\{Store, Category, Sale, SaleDetail, SalesItems, Helper, DeliveryPrice};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class WebController extends Controller
{
    public function home() {
        $user = Auth::user();
        $stores = Store::where('is_salient', 1)->get();
        $categories = Category::where('type', 1)->get();
        return view('web.home', ['stores' => $stores, 'categories' => $categories, 'user' => $user]);
    }

    public function about() {
        return view('web.about');
    }

    public function termsConditions() {
        return view('web.terms_conditions');
    }

    public function submitRestaurant() {
        return view('web.submit_restaurant');
    }

    public function submitDriver() {
        return view('web.submit_driver');
    }
    
    public function perfil() {
        #TODO revisar logica
        $user = Auth::user();
        return view('web.perfil', ['user' => $user]);
    }

    public function testPy() {
        $command = escapeshellcmd('python /home/averdugo/Codes/laravel/compraaca/public/scripts/test.py -33.5767603 -70.4493518 -33.7297468 -70.3260183');
        $output = shell_exec($command);

        dd($output);
    }

    public function getDeliveryPrice($distance) {
        
        $dt = Carbon::now();
        $deliveryPrice = "";
        
        switch ($dt->hour) {
            case ($dt->hour <= 21 && $dt->hour >= 8):
                $deliveryPrice = DeliveryPrice::where('type', 1)->first();
                break;
                case ($dt->hour >= 21 || $dt->hour <= 8):
                $deliveryPrice = DeliveryPrice::where('type', 2)->first();
                break;
        }
        $kmDistance = round($distance / 1000);
        
        $amount = 0;
        switch ($kmDistance) {
            case ($kmDistance <= 14 ):
                $amount = $deliveryPrice->amount;
                break;
            case ($kmDistance > 14 &&  $kmDistance < 40 ):
                $amount = $deliveryPrice->amount2;
                break;
            case ($kmDistance >= 40 ):
                $amount = $deliveryPrice->amount3;
                break;
        }
        $kmPrice = $amount * $kmDistance;
        $price =  $deliveryPrice->point + $kmPrice;
        return $price > $deliveryPrice->minimum ? round($price) : $deliveryPrice->minimum;
    }
}
