<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Illuminate\Http\Request;
use App\Jobs\SendContactEmail;
use App\Jobs\SendContactDriverEmail;
use App\Jobs\SendContactRestaurantEmail;

class MailController extends Controller
{

    public function contactMail(Request $request) {
        
        $data = $request->all();
        
        SendContactEmail::dispatch($data);

        return back()->with('status', 'Hemos recibido su email pronto nos contactaremos con usted!.');

    }
    public function restorantRequestMail(Request $request) {

        $data = $request->all();
        
        SendContactRestaurantEmail::dispatch($data);

        return back()->with('status', 'Hemos recibido su email pronto nos contactaremos con usted!.');

    }
    public function deliveryRequestMail(Request $request) {
        
        $data = $request->all();
        
        SendContactDriverEmail::dispatch($data);

        return back()->with('status', 'Hemos recibido su email pronto nos contactaremos con usted!.');

    }

    public function salestMail() {
        
    }

    
}
