<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException;



 class Config {

	static function get($name) {
		
		$COMMERCE_CONFIG = array(
			"APIKEY" => "1F0EB550-835A-4178-9C5A-807E2DFAL4B5", // Registre aquí su apiKey
			"SECRETKEY" => "7d36eb71a5d6404cbbc344f919161f51b47bf12f", // Registre aquí su secretKey
			"APIURL" => "https://www.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
			//"BASEURL" => "http://167.99.118.90/apiFlow" //Registre aquí la URL base en su página 
			"BASEURL" => "http://compraaca.com/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
		 );

		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new ModelNotFoundException("The configuration element thas not exist", 1);
		}
		return $COMMERCE_CONFIG[$name];
	}
 }
