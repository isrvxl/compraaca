<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sale;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $status = Sale::STATUS;
		if ($user->type == 1) {
			return view('admin.dashboard', ['status' => $status, 'menu' => 1]);

        }
        else if ($user->type == 2 || $user->type == 3) {
			return back();
		}
    }


}


