<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ProductsImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\StoreProductPost;
use App\{Product, Store, Sale, Category, Type};

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id','asc')->get();
        $stores = Store::where('status', 1)->get();
        $categories = Category::where('type', 2)->get();
        $types = Type::where('type', 2)->get();
        $sale = Sale::orderBy('created_at','asc')->get();
        $status = Sale::STATUS;
        return view('admin.products.list', ['menu' => 3, 'sale' => $sale, 'status' => $status,'stores'=>$stores,'products' => $products, 'categories' => $categories, 'types' => $types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create', ['product'=> new Product()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['inventary'] = ( isset($data['inventary']) && $data['inventary'] == 'on') ? 1 : 0;
        
        $product = Product::create($data);

        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_product'), $filename);
			$product->img = $filename;
			$product->save();
        }

        return back()->with('status', 'Producto creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return Product::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->all();
        $data['inventary'] = ( isset($data['inventary']) && $data['inventary'] == 'on') ? 1 : 0;

        $product->update($data);
              
        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_product'), $filename);
			$product->img = $filename;
			$product->save();
        }
        
        return back()->with('status', 'Producto actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return back()->with('status', 'Producto eliminado con exito');
    }

    public function import(Request $request){
        (new ProductsImport($request->store_id))->import(request()->file('name'));

        return back()->with('status', 'Importado con exito');
    }
}
