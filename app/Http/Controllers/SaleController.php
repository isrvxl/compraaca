<?php

namespace App\Http\Controllers;

use Auth;
use App\Jobs\SendEmail;
use App\Jobs\SendInfoPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\FlowApi;
use Illuminate\Support\Facades\Hash;
use App\{Sale, Store, User, SaleDetail, SalesItems};

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $sale = Sale::where('status',1)->get();
        $status = Sale::STATUS;
        return view('admin.sales.list', ['menu' => 4, 'sale' => $sale, 'sale' => $sale,'status' => $status]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $sale = Sale::create ([ 
            'user_id' => Auth::user() ? Auth::user()->id : null ,
            'amount' => $request->amount, 
            'store_id' => $request->store_id, 
            'status' => 0, 
        ]);

        foreach($request->items as $key => $i){
            $saleItems = SalesItems::create ([
                'product_id' => $i['id'],
                'qty' => $i['qty'], 
                'amount' => $i['totalPrice'], 
                'sale_id' => $sale->id, 
            ]);
        }

        return $sale;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Sale::with('user', 'store', 'saleitems', 'details', 'saleitems.product')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        return Sale::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        $sale->update($request->all());

        return back()->with('status', 'Venta actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }

    public function detailCart(Request $request){
        $request->validate([
            'email_order'       => 'required',
            'tel_order'    => 'required',
            'address' => 'required',
            'delivery_price'=> 'required',
        ]);
        if(Auth::user()) {
            $user = Auth::user();
        }else {
            $user = User::create([
                'f_name' => $request->f_name, 
                'l_name' => $request->l_name,
                'email'=> $request->email_order, 
                'type' => 3, 
                'phone'  => $request->tel_order,
                'rut'  => $request->rut,
                'password' => Hash::make($request->password),
            ]);

            Auth::attempt(['email' => $user->email, 'password' => $request->password]);

            SendEmail::dispatch($user->f_name, $user->l_name, $user->email, $user->phone, $user->rut, $request->password);
        }

        $saleDetail = SaleDetail::create([
            'delivery_address' => $request->address,
            'contact_name' => $request->f_name ." ". $request->l_name,
            'contact_phone' => $request->tel_order,
            'description' => $request->notes,
            'sale_id' =>  $request->sale_id,  
            'user_id' => $user->id,
        ]);

        $sale = Sale::find($request->sale_id);
        //dd($request->all());
        $sale->delivery_fee = $request->delivery_price;
        $sale->user_id = $user->id;
        $sale->update();

        $stores = Store::find($request->store_id);
        return redirect('sale/'.$sale->id.'/payment');
        
    }

    public function orderPayment($id)
    {
        $user = Auth::user();
        $sale = Sale::find($id);
        return view('web.detail_payment', ['user' => $user , 'sale' => $sale]);
    }

    public function goFlow($id) {
        $user = Auth::user();
        $sale = Sale::find($id);
        $url = url('/');
        $optional = array(
			"rut" => $user->rut,
			"id" => $sale->id,
		);

        $optional = json_encode($optional);
        
        $params = array(
			"commerceOrder" => $sale->id,
			"subject" => "Pago en Compraaca",
			"currency" => "CLP",
			"amount" => intval($sale->amount) + intval($sale->delivery_fee),
			"email" => $user->email,
			"paymentMethod" => 9,
			"urlConfirmation" =>  $url."/flowConfirm",
			"urlReturn" => $url."/flowResult",
			"optional" => $optional
        );
        
        $serviceName = "payment/create";
		$flowApi = new FlowApi();
		$response = $flowApi->send($serviceName, $params,"POST");
		$redirect = $response["url"] . "?token=" . $response["token"];

		return redirect($redirect);

    }

    public function saleTest() {
        $optional = array(
			"rut" => '162507745',
			"id" => 30,
		);

		$optional = json_encode($optional);

		$params = array(
			"commerceOrder" => 300,
			"subject" => "Pago en Compraaca",
			"currency" => "CLP",
			"amount" => 800,
			"email" => 'aldo.vc.1985@gmail.com',
			"paymentMethod" => 9,
			"urlConfirmation" =>  "http://127.0.01:8000/flowConfirm",
			"urlReturn" => "http://127.0.01:8000/flowResult",
			"optional" => $optional
		);

		$serviceName = "payment/create";
		$flowApi = new FlowApi();
		$response = $flowApi->send($serviceName, $params,"POST");
		$redirect = $response["url"] . "?token=" . $response["token"];

		return redirect($redirect);
    }

    public function confirm(Request $request){
        try {
            if(!isset($_POST["token"])) {
                throw new Exception("No se recibio el token", 1);
            }
            $token = filter_input(INPUT_POST, 'token');
            $params = array(
                "token" =>  $request->token
            );
            $serviceName = "payment/getStatus";
            $flowApi = new FlowApi();
            $response = $flowApi->send($serviceName, $params, "GET");
            
            //Actualiza los datos en su sistema
            
            print_r($response);
            
            
        } catch (Exception $e) {
            echo "Error: " . $e->getCode() . " - " . $e->getMessage();
        }
    }

    public function flowConfirm(Request $request)
	{

		try {
			if(!isset($_POST["token"])) {
				throw new Exception("No se recibio el token", 1);
			}
			$token = filter_input(INPUT_POST, 'token');
			$params = array(
				"token" => $request->token
			);
			$serviceName = "payment/getStatus";
			$flowApi = new FlowApi();
			$response = $flowApi->send($serviceName, $params, "GET");
			$sale = Sale::with('user', 'store', 'saleitems', 'details', 'saleitems.product')->find($response['optional']['id']);
			if ($response['status'] == 2) {
                SendInfoPayment::dispatch($sale);
				$sale->status = 1;
                $sale->save();
                return redirect('sale/'.$sale->id.'/confirmed');
			}else{
				$sale->status = 2;
				$sale->save();
                return redirect('sale/'.$sale->id.'/rejected');
			}
		} catch (Exception $e) {
			echo "Error: " . $e->getCode() . " - " . $e->getMessage();
		}
	}
	public function flowResult(Request $request)
	{

		try {
			if(!isset($_POST["token"])) {
				throw new Exception("No se recibio el token", 1);
			}
			$token = filter_input(INPUT_POST, 'token');
			$params = array(
				"token" => $request->token
			);
			$serviceName = "payment/getStatus";
			$flowApi = new FlowApi();
			$response = $flowApi->send($serviceName, $params, "GET");
            //dd($response);
			$sale = Sale::with('user', 'store', 'saleitems', 'details', 'saleitems.product')->find($response['optional']['id']);
			if ($response['status'] == 2) {
                SendInfoPayment::dispatch($sale);
				$sale->status = 1;
                $sale->save();
                return redirect('sale/'.$sale->id.'/confirmed');
				
			}else{
				$sale->status = 2;
				$sale->save();
                return redirect('sale/'.$sale->id.'/rejected');
			}
		} catch (Exception $e) {
			echo "Error: " . $e->getCode() . " - " . $e->getMessage();
		}
    }

    public function orderConfirmed($id)
    {
        $user = Auth::user();
        $sale = Sale::find($id);
        return view('web.detail_confirmed', ['user' => $user, 'sale' => $sale]);
    }

    public function orderRejected($id)
    {
        $user = Auth::user();
        $sale = Sale::find($id);
        return view('web.detail_rejected', ['user' => $user, 'sale' => $sale]);
    }

    public function invoice($id){
        $sale = Sale::find($id);
        return view('admin.sales.saleView', ['menu' => 4, 'sale' => $sale]);
    }

    public function invoicePerfil($id){
        $sale = Sale::find($id);
        return view('web.saleView', ['sale' => $sale]);
    }

}
