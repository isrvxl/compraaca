<?php

namespace App\Http\Controllers;

use App\{User, Store, Sale};
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $stores = Store::all();
        $types = User::TYPES;
        $sale = Sale::orderBy('created_at','asc')->get();
        $status = Sale::STATUS;
        return view('admin.users.list', ['menu' => 5, 'sale' => $sale, 'status' => $status, 'users' => $users, 'stores'=>$stores,'types' => $types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create', ['user'=> new User()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create($request->all());

        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_user'), $filename);
			$user->img = $filename;
			$user->save();
        }

        return back()->with('status', 'Usuario creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return User::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        if ($request->file('img')) {
            $filename = time() . "." . $request->img->extension();
            $request->img->move(public_path('images_user'), $filename);
			$user->img = $filename;
			$user->save();
        }
        
        return back()->with('status', 'Usuario actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return back()->with('status', 'Usuario eliminado con exito');
    }
}
