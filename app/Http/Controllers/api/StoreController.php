<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\{Store,Type,Product};

class StoreController extends ApiResponseController
{   
    public function listTypesStores(){
        $types = Type::where('type', 1)->get();
        return $this->successResponse($types);
    }

    public function listAllStores($lat, $lng){
        $stores = Store::where('status', 1)->get();
        $mapStore = Store::dataForApp($stores, $lat, $lng);
        return $this->successResponse($mapStore);
    }

    public function listStoresByCategory($category, $lat, $lng){
        
        $stores = Store::where('category_id', $category)->where('status', 1)->get();
        $data = Store::dataForApp($stores, $lat, $lng);
        return $this->successResponse($data);
    }

    public function listStoresByTypes($type, $lat, $lng){
        $stores = Store::where('type', $type)->where('status', 1)->get();
        $data = Store::dataForApp($stores, $lat, $lng);
        return $this->successResponse($data);
    }

    public function detailsStore($id)
    {
        $products = Product::where('store_id', $id)->orderBy('type')->get();

        foreach ($products as $key => $p) {
            $products[$key]['categoryName'] = $p->category->name;
            $products[$key]['tipoName'] = $p->tipo->name;
        }
        
        return $this->successResponse($products);
    }
        
    
}
