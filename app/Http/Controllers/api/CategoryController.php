<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\{Category,Type,Product};

class CategoryController extends ApiResponseController
{   
    public function listCategory(){
        $categories = Category::where('type', 1)->get();
        return $this->successResponse($categories);
    }

    public function listType(){
        $categories = Type::where('type', 1)->get();
        return $this->successResponse($categories);
    }

}
