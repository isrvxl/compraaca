<?php

namespace App;

use App\Subscription;
use Laravel\Passport\HasApiTokens;
use App\Notifications\UserResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
   // use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name', 'l_name','email', 'password', 'type', 'img', 'phone', 'address', 'locality', 'city', 'rut', 
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function sales()
    {
        return $this->hasMany('App\Sale');
    }

    const TYPES = [
        1 => 'Admin',
        2 => 'Cliente',
        3 => 'Usuario',
        4 => 'Repartidor'
    ];

   

    public function getTypelabelAttribute(){
        return Self::TYPES[$this->type];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }
}
