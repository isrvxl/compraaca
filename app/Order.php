<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'sale_id',
        'user_id',
        'delivery_id',
        'store_id',
        'status',
        'address',
        'lat',
        'lng',
    ];

    public function sale() {
        $this->belongsTo('App\Sale');
    }

    public function user() {
        $this->belongsTo('App\User');
    }

}
