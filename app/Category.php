<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'type', 'img'];

    const TYPES = [
        1 => 'Tienda',
        2 => 'Producto',
    ];

   

    public function getTypelabelAttribute(){
        return Self::TYPES[$this->type];
    }
}
