<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    protected $fillable = [
        'delivery_address',
        'contact_name',
        'contact_phone',
        'description',
        'sale_id',
        'user_id'
    ];

    public function sale() {
       return  $this->belongsTo('App\Sale');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
