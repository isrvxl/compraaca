<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'user_id',
        'store_id',
        'amount',
        'status',
        'delivery_fee'
    ];

    public function store() {
        return $this->belongsTo('App\Store');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function saleitems() {
        return $this->hasMany('App\SalesItems');
    }
    
    public function details() {
        return $this->hasOne('App\SaleDetail');
    }

    const STATUS= [
		9 =>'Pendiente',
		0 => 'Creado/Pendiente',
		1 =>'Pagado',
		2 =>'Rechazado'
    ];
    
    public function getStatuslabelAttribute(){
        return Self::STATUS[$this->status];
    }

}
