<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['store_id', 'name', 'description', 'img', 'price', 'discount_price', 'weigth', 'dimention', 'high', 'long', 'type', 'category_id', 'status', 'stock', 'inventary'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function tipo()
    {
        return $this->belongsTo('App\Type', 'type');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
