<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\{Importable, ToModel, WithHeadingRow};


class ProductsImport implements ToModel, WithHeadingRow
{

    use Importable;
    private $storeId;

    public function __construct($storeId) 
    {
        $this->storeId = $storeId;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        return new Product([
            'store_id' => $this->storeId,
            'name' => $row['nombre'],
            'description' => $row['descripcion'],
            'price' => $row['precio'],
            'discount_price' => $row['precio_descuento'],
            'weigth' => $row['peso'],
            'dimention' => $row['dimencion'],
            'high' => $row['alto'],
            'long' => $row['largo'],
            'type' => $row['categoria'],
            'category_id' => $row['tipo'],
            'status' => $row['estado'],
        ]);
    }
}
