<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesItems extends Model
{
    protected $fillable = [
        'product_id',
        'qty',
        'sale_id',
        'amount'
    ];

    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function sale() {
        return $this->belongsTo('App\Sale');
    }

}

