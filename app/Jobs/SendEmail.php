<?php

namespace App\Jobs;

use App\Mail\EmailForQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $f_name;
    protected $l_name;
    protected $email_order;
    protected $tel_order;
    protected $rut;
    protected $password;
   

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($f_name, $l_name, $email_order, $tel_order, $rut, $password)
    {
        $this->f_name = $f_name;
        $this->l_name = $l_name;
        $this->email_order = $email_order;
        $this->tel_order = $tel_order;
        $this->rut = $rut;
        $this->password = $password;
       
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new EmailForQueue($this->f_name, $this->l_name,  $this->email_order, $this->tel_order, $this->rut, $this->password);
        Mail::to($this->email_order)->send($email);
    }
}
